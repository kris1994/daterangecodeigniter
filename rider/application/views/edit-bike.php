

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">


            <!-- end row -->


            <div class="row">
                <div class="col-12">
                    <div class="card-box">



                        <div class="row">


                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12 m-t-sm-40">

                                <form method="post" action="<?= base_url() ?>admin/editBikeInfo/<?= $location[0]['bID'] ?>" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-6">
                                            <fieldset disabled>
                                                <div class="form-group">
                                                    <label for="disabledTextInput">Biker ID</label>
                                                    <input type="text" id="disabledTextInput" class="form-control"
                                                           value="<?= $location[0]['bID'] ?>">
                                                </div>

                                            </fieldset>
                                        </div>
                                        <div class="col-6">
                                            <fieldset class="form-group">
                                                <label for="TextInput3">Vehicle Number</label>
                                                <input type="text" class="form-control" id="TextInput3"
                                                       placeholder="BCK 1245" name="vehicle_number" required>

                                            </fieldset>
                                        </div>
                                        <div class="col-6">
                                            <fieldset class="form-group">
                                                <label for="TextInput">Brand</label>
                                                <input type="text" class="form-control" id="TextInput"
                                                       placeholder="Honda" name="brand" required >

                                            </fieldset>
                                        </div>
                                        <div class="col-6">
                                            <fieldset class="form-group">
                                                <label for="TextInput4">Chassis Number</label>
                                                <input type="text" class="form-control" id="TextInput4"
                                                       placeholder="1DGCM25899S564875" name="chassis_number" required>

                                            </fieldset>
                                        </div>
                                        <div class="col-6">
                                            <fieldset class="form-group">
                                                <label for="TextInput2">Type</label>
                                                <input type="text" class="form-control" id="TextInput2"
                                                       placeholder="Navi" name="type" required>

                                            </fieldset>
                                        </div>
                                        <div class="col-6">
                                            <fieldset>
                                                <label for="TextInput4">Insurance Expiry</label>
                                                <div class="input-group">

                                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" name="insurance_exp" id="datepicker-autoclose" required>
                                                    <span class="input-group-addon bg-custom b-0"><i class="icon-calender"></i></span>
                                                </div><!-- input-group -->
                                            </fieldset>

                                        </div>
                                        <div class="col-6">
                                            <fieldset class="form-group">
                                                <label for="exampleSelect1">Assign Rider</label>
                                                <select name="internal-rider" class="form-control" id="exampleSelect1">
                                                    <?php foreach ($internal_riders as $row ): ?>
                                                        <option <?php if($row['rName']){echo 'selected';} ?> value="<?= $row['rName'] ?>"><?= $row['rName'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </fieldset>

                                        </div>

                                    </div>

                                    <div class="form-group text-right m-b-0">
                                        <button class="btn btn-cancel  waves-effect waves-light" type="reset">
                                            Cancel
                                        </button>
                                        <button id="add-loc-btn" class="btn  btn-send btn btn-save waves-effect m-l-5" type="submit">Save</button>
                                        <!--                                        <button type="reset" class="btn btn-save waves-effect m-l-5">-->
                                        <!--                                            Save-->
                                        <!--                                        </button>-->
                                    </div>
                                </form>

                            </div><!-- end col -->

                        </div><!-- end row -->
                    </div>
                </div><!-- end col -->
            </div>

        </div> <!-- container -->

    </div> <!-- content -->



</div>
<!-- End content-page -->


<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->


<!-- Right Sidebar -->
<div class="side-bar right-bar">
    <div class="nicescroll">
        <ul class="nav nav-pills nav-justified text-xs-center">
            <li class="nav-item">
                <a href="#home-2"  class="nav-link active" data-toggle="tab" aria-expanded="false">
                    Activity
                </a>
            </li>
            <li class="nav-item">
                <a href="#messages-2" class="nav-link" data-toggle="tab" aria-expanded="true">
                    Settings
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade active show" id="home-2">
                <div class="timeline-2">
                    <div class="time-item">
                        <div class="item-info">
                            <small class="text-muted">5 minutes ago</small>
                            <p><strong><a href="#" class="text-info">John Doe</a></strong> Uploaded a photo <strong>"DSC000586.jpg"</strong></p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <small class="text-muted">30 minutes ago</small>
                            <p><a href="" class="text-info">Lorem</a> commented your post.</p>
                            <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <small class="text-muted">59 minutes ago</small>
                            <p><a href="" class="text-info">Jessi</a> attended a meeting with<a href="#" class="text-success">John Doe</a>.</p>
                            <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <small class="text-muted">1 hour ago</small>
                            <p><strong><a href="#" class="text-info">John Doe</a></strong>Uploaded 2 new photos</p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <small class="text-muted">3 hours ago</small>
                            <p><a href="" class="text-info">Lorem</a> commented your post.</p>
                            <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <small class="text-muted">5 hours ago</small>
                            <p><a href="" class="text-info">Jessi</a> attended a meeting with<a href="#" class="text-success">John Doe</a>.</p>
                            <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="messages-2">

                <div class="row m-t-10">
                    <div class="col-8">
                        <h5 class="m-0">Notifications</h5>
                        <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                    </div>
                    <div class="col-4 text-right">
                        <input type="checkbox" checked data-plugin="switchery" data-color="#1bb99a" data-size="small"/>
                    </div>
                </div>

                <div class="row m-t-10">
                    <div class="col-8">
                        <h5 class="m-0">API Access</h5>
                        <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                    </div>
                    <div class="col-4 text-right">
                        <input type="checkbox" checked data-plugin="switchery" data-color="#1bb99a" data-size="small"/>
                    </div>
                </div>

                <div class="row m-t-10">
                    <div class="col-8">
                        <h5 class="m-0">Auto Updates</h5>
                        <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                    </div>
                    <div class="col-4 text-right">
                        <input type="checkbox" checked data-plugin="switchery" data-color="#1bb99a" data-size="small"/>
                    </div>
                </div>

                <div class="row m-t-10">
                    <div class="col-8">
                        <h5 class="m-0">Online Status</h5>
                        <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                    </div>
                    <div class="col-4 text-right">
                        <input type="checkbox" checked data-plugin="switchery" data-color="#1bb99a" data-size="small"/>
                    </div>
                </div>

            </div>
        </div>
    </div> <!-- end nicescroll -->
</div>
<!-- /Right-bar -->



</div>
<!-- END wrapper -->


<script>
    var resizefunc = [];
</script>
<!-- jQuery  -->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/detect.js"></script>
<script src="<?php echo base_url(); ?>assets/js/fastclick.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/moment/moment.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/mjolnic-bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/clockpicker/bootstrap-clockpicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="<?php echo base_url(); ?>assets/pages/jquery.form-pickers.init.js"></script>

<!-- App js -->
<script src="<?php echo base_url(); ?>assets/js/jquery.core.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.app.js"></script>


</body>
</html>