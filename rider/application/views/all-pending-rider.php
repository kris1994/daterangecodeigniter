
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <?php
            $segment = $this->uri->segment(3);
            if($segment == null) {
                $table_num_rows = 0;
                $showing = 1;
            } else if($segment == 1) {
                $table_num_rows = 0;
                $showing = 1;
            }elseif($segment > 1){
                $pgs= $segment - 1;
                $table_num_rows = 10 * $pgs;
                $showing = 10 * $pgs;
            }
            ?>
            <!-- end row -->


            <section class="content-header">
                <ul class="list-inline menu-left mb-0">
                    <form autocomplete="off" id="search-all-form" method="post" action="<?= base_url() ?>admin/search-user/1">
                    <li class="hidden-mobile app-search">
                        <div class="input-group custom-input-group">

                            <input type="hidden" name="search_param" value="name" id="search_param">
                            <input name="search" autocomplete="off" id="search-user" class="form-control" placeholder="Search User">
<!--                            <input type="text" class="form-control" name="q" placeholder="Search.." id="search_key" value="">-->

                            <button type="submit" class="button-insider-search"><i class="fa fa-search"></i></button>
                        </div>
                    </li>
                    </form>
                </ul>
                <ol class="breadcrumb">
                    <div class="col-sm-8">




                <form id="sort-submits" method="post" action="<?= base_url() ?>admin/sortLocation/1">
                    <div class="input-group input-daterange ">
                        <span class="input-group-addon daterange-div text-sm-dt">Date Joined</span> <input id="startDate1" name="startDate1" type="text"
                               class="form-control dateranger" readonly="readonly" placeholder="From"> <span
                            class="input-group-addon"> <span
                                class="zmdi zmdi-calendar color-change"></span>
		</span>  <input id="endDate1"
                                                                 name="endDate1" type="text" class="form-control dateranger" readonly="readonly" placeholder="To">
                        <span class="input-group-addon daterange-span-radius"> <span
                                class="zmdi zmdi-calendar color-change"></span>
		</span>
                    </div>

                </form>
                    </div>
                </ol>
               
            </section>
            <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive table-margin">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr class="div-bottom-line">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Contact Number</th>
                                <th>Email</th>
                                <th>No of Documents</th>
                                <th class="display-none-div action-th">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php if (count($all_riders) != 0): ?>
                                <?php foreach ($all_riders as $value):
                                ?>

                                    <tr>
                                        <td>
                                            <?php echo $value['rID'] ?>
                                        </td>

                                        <td>
                                            <?php
                                            if($value['rName']){
                                                echo $value['rName'];
                                            }else{
                                                echo '-';
                                            }?>
                                        </td>
                                        <td>
                                            <?php
                                            if($value['rPhone']){
                                                echo $value['rPhone'];
                                            }else{
                                                echo '-';
                                            }?>
                                        </td>
                                        <td>
                                            <?php
                                            if($value['rEmail']){
                                                echo $value['rEmail'];
                                            }else{
                                                echo '-';
                                            }?>
                                        </td>
                                        <td>
                                            <?php
                                            if($value['no_of_documents']){
                                                echo $value['no_of_documents'];
                                                // echo count(@$res);
                                            }else{
                                                echo '-';
                                            }?>
                                        </td>
                                        <td class="tbl-action">
                                            <a title="pending" href="<?php echo base_url(); ?>admin/action-pending-rider/<?php echo $value['rID'];?>"><i class="zmdi zmdi-book"></i></a>
                                        </td>

                                    </tr>
                                    <?php $table_num_rows++; ?>
                                <?php endforeach ?>
                                <?php
                                $detail_show = false;
                                ?>
                            <?php else: ?>
                                <?php
                                $detail_show = true;
                                ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <div class="row">
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end row -->

    </div> <!-- container -->

</div> <!-- content -->



</div>
<!-- End content-page -->




</div>
<!-- END wrapper -->
<?php if($total_db_rows >10){ ?>
    <script>
        $(function() {
            $('#example2_paginate').pagination({
                items: <?= $total_db_rows ?>,
                itemsOnPage: 10,
                cssStyle: 'compact-theme',
                hrefTextPrefix: '',
                displayedPages: 3

            });
            $('#example2_paginate').pagination('drawPage', <?= $segment ?>);
        });
    </script>
<?php } ?>
<!-- END wrapper -->
<?php if($total_db_rows >10){ ?>
    <script>
        $(function() {
            $('#example2_paginate').pagination({
                items: <?= $total_db_rows ?>,
                itemsOnPage: 10,
                cssStyle: 'compact-theme',
                hrefTextPrefix: '',
                displayedPages: 3

            });
            $('#example2_paginate').pagination('drawPage', <?= $segment ?>);
        });
    </script>
<?php } ?>


<script>
    $(document).ready(function () {
        $('.datepicker').datetimepicker({
            endDate: new Date(),
            weekStart: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: 'yyyy-mm-dd'
        });
        $('#sort-user').on('change', function(){
            var val = $(this).val();
            $('form#sort-submits').submit();
        });
        $('#sort-by-active').on('change', function(){
            var val = $(this).val();
            $('form#sort-submit-active').submit();
        });


        $("#search-user").on('keyup', function () {
            var user = $('#search-user').val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "admin/searchUserAutoComplete",
                data: {user: user},
                success: function (res) {
                    $("#searchResult").html(res);
                    $("#searchResult").slideDown();
                }
            });
        });
        $('#searchResult').on('click', '.search-text',function () {
            var searchKey = $(this).attr("data-value");
            $("#search-user").val(searchKey);
            $('form#search-all-form').trigger('submit');

        });

    });
</script>


<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/detect.js"></script>
<script src="<?php echo base_url(); ?>assets/js/fastclick.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.js"></script>

<!-- Required datatable js -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/buttons.colVis.min.js"></script>
<!-- Responsive examples -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/responsive.bootstrap4.min.js"></script>

<!-- App js -->
<script src="<?php echo base_url(); ?>assets/js/jquery.core.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.app.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();

        //Buttons examples
        var table = $('#datatable-buttons').DataTable({
            lengthChange: false,
            buttons: ['copy', 'excel', 'pdf', 'colvis']
        });

        table.buttons().container()
            .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
    } );

</script>




<script
    src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.input-daterange input').each(function() {
            $(this).datepicker();
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('.datepicker').datetimepicker({
            endDate: new Date(),
            weekStart: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: 'yyyy-mm-dd'
        });
        $('#sort-user').on('change', function(){
            var val = $(this).val();
            $('form#sort-submits').submit();
        });
        $('#sort-by-active').on('change', function(){
            var val = $(this).val();
            $('form#sort-submit-active').submit();
        });


        $("#search-user").on('keyup', function () {
            var user = $('#search-user').val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "admin/searchUserAutoComplete",
                data: {user: user},
                success: function (res) {
                    $("#searchResult").html(res);
                    $("#searchResult").slideDown();
                }
            });
        });
        $('#searchResult').on('click', '.search-text',function () {
            var searchKey = $(this).attr("data-value");
            $("#search-user").val(searchKey);
            $('form#search-all-form').trigger('submit');

        });

    });
</script>



</body>
</html>