function zoomin(){
        var myImg = document.getElementById("sky");
        var currWidth = myImg.clientWidth;
        var currHeight = myImg.clientHeight;
        if(currWidth == 500 || currHeight == 500){
            alert("Maximum zoom-in level reached.");
        } else{
            myImg.style.width = (currWidth + 50) + "px";
            myImg.style.height = (currHeight + 50) + "px";
        } 
    }
    function zoomout(){
        var myImg = document.getElementById("sky");
        var currWidth = myImg.clientWidth;
        var currHeight = myImg.clientHeight;
        if(currWidth == 50 ||currHeight == 50){
            alert("Maximum zoom-out level reached.");
        } else{
            myImg.style.width = (currWidth - 50) + "px";
            myImg.style.height = (currHeight - 50) + "px";
        }
    }