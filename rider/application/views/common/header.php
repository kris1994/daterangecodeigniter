<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">

    <!-- App title -->
    <title>rider</title>

    <!-- DataTables -->
    <link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="<?php echo base_url(); ?>assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Switchery css -->
    <link href="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.css" rel="stylesheet" />

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <script
        src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script
        src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.min.css">
    <!-- Switchery css -->
    <link href="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
    <!-- Plugins css-->
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script src="https://code.jquery.com/jquery-1.11.3.js"></script>

    <!-- form Uploads -->
    <link href="<?php echo base_url(); ?>assets/plugins/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/css/sweetalert.css" rel="stylesheet" type="text/css" />
    <!-- Plugins css -->
    <link href="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plugins/mjolnic-bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plugins/clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- C3 charts css -->
    <link href="<?php echo base_url(); ?>assets/plugins/c3/c3.min.css" rel="stylesheet" type="text/css"  />
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/morris/morris.css">

    <!-- Switchery css -->
    <link href="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.css" rel="stylesheet" />

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/simplePagination.css">
    <!-- App CSS -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- Modernizr js -->

    <script src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>
    <script>
        var base_url = "<?php echo base_url(); ?>";
    </script>
</head>


<body class="fixed-left body-main">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <a href="<?php echo base_url(); ?>admin/dashboard" class="logo">

                <span><img src="<?php echo base_url(); ?>assets/images/smlogo.png"></span></a>
        </div>
        <nav class="navbar-custom">
           <!--  <ul class="list-inline float-right mb-0">
               
            </ul> -->
            <ul class="list-inline float-right mb-0">


                <li class="list-inline-item">
                     <form autocomplete="off" id="search-all-form" method="post" action="<?php echo base_url() ?>admin/search-bike-rider/1">
                        <li class="hidden-mobile app-search">
                            <div class="input-group custom-input-group">
                                <select name="search_category" id="search_category" class="form-control" placeholder="Search User">
                                    <option value="bikes" <?php echo @($this->input->post('search_category') == 'bikes') ? 'selected' : ''?> >Bikes</option>
                                    <option value="riders" <?php echo @($this->input->post('search_category') == 'riders') ? 'selected' : ''?> >Riders</option>
                                </select>
                                <input type="hidden" name="search_param" value="name" id="search_param">
                                <input name="search" autocomplete="off" id="search-user" class="form-control" value="<?php echo $this->input->post('search')?>" placeholder="Search Bikes/Riders">
                                <!-- <button type="submit" class="button-insider-search"><i class="fa fa-search"></i></button> -->
                            </div>

                        </li>
                    </form>
                </li>
                <li class="list-inline-item dropdown notification-list notification-border">
                    <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                       aria-haspopup="false" aria-expanded="false">
                        <img src="<?php echo base_url(); ?>assets/images/users/user-image-menu.png" alt="user" class="rounded-circle">
                        <span class="admin_username hidden-xs"><?= $this->session->userdata['admin_username'] ?></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                        <!-- item-->
                        <div class="dropdown-item noti-title">
                            <h5 class="text-overflow"><small>Welcome ! <?= $this->session->userdata['admin_username'] ?></small> </h5>
                        </div>

                        <!-- item-->
                        <a href="<?php echo base_url(); ?>change-password" class="dropdown-item notify-item">
                            <i class="zmdi zmdi-account-circle"></i> <span>Change Password</span>
                        </a>

<!--                        <!-- item-->
<!--                        <a href="javascript:void(0);" class="dropdown-item notify-item">-->
<!--                            <i class="zmdi zmdi-settings"></i> <span>Settings</span>-->
<!--                        </a>-->
<!---->
<!--                        <!-- item-->
<!--                        <a href="javascript:void(0);" class="dropdown-item notify-item">-->
<!--                            <i class="zmdi zmdi-lock-open"></i> <span>Lock Screen</span>-->
<!--                        </a>-->

                        <!-- item-->
                        <a href="<?= base_url() ?>admin/logout" class="dropdown-item notify-item">
                            <i class="zmdi zmdi-power"></i> <span>Logout</span>
                        </a>

                    </div>
                </li>
                <li class="list-inline-item dropdown notification-list ">
                    <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown" href="#" role="button"
                       aria-haspopup="false" aria-expanded="false">
                        <i class="zmdi zmdi-notifications noti-icon"></i>
                        <span class="noti-icon-badge"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-lg" aria-labelledby="Preview">
                        <!-- item-->
                        <div class="dropdown-item noti-title">
                            <h5><small><span class="label label-danger pull-xs-right">7</span>Notification</small></h5>
                        </div>

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>
                            <p class="notify-details">Robert S. Taylor commented on Admin<small class="text-muted">1min ago</small></p>
                        </a>

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <div class="notify-icon bg-info"><i class="icon-user"></i></div>
                            <p class="notify-details">New user registered.<small class="text-muted">1min ago</small></p>
                        </a>

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <div class="notify-icon bg-danger"><i class="icon-like"></i></div>
                            <p class="notify-details">Carlos Crouch liked <b>Admin</b><small class="text-muted">1min ago</small></p>
                        </a>

                        <!-- All-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item notify-all">
                            View All
                        </a>

                    </div>
                </li>
            </ul>

            <ul class="list-inline menu-left mb-0">
                <div>
                    <h4 class="page-title float-left">Dashboard</h4>


                    <div class="clearfix"></div>
                </div>

            </ul>

        </nav>

    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <ul class="list-inline menu-left mb-30">
                    <li class="float-right">
                        <button class="button-menu-mobile open-left waves-light waves-effect">
                            <i class="ion-chevron-left"></i>
                        </button>
                    </li>

                </ul>
                <ul>


                    <li class="has_sub">
                        <a href="<?php echo base_url(); ?>admin/dashboard" class="waves-effect"><i class="zmdi zmdi-equalizer"></i><span> Dashboard </span> </a>
                    </li>

                    <li class="has_sub">
                        <a href="<?php echo base_url(); ?>admin/all-bikes/1" class="waves-effect"><i class="zmdi zmdi-bike"></i><span> Bikes</span> </a>

                    </li>


                    <li class="has_sub">
                        <a href="<?php echo base_url(); ?>admin/all-riders/1" class="waves-effect"><i class="zmdi zmdi-accounts"></i> <span> Riders </span> </a>

                    </li>

                    <li class="has_sub">
                        <a href="<?php echo base_url('admin/pending-rider/1');?>" class="waves-effect"><i class="zmdi zmdi-hourglass"></i><span> Pending <span class="label label-warning pull-xs-right"><?php
                        $rdocID = $this->admin_model->getAllRiderStatus3();
                        $where = $rdocID;
                        echo count($this->admin_model->getAllRidersPending(0, '20000',$where))?></span></a>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-local-atm"></i> <span> Payments </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="widgets-tiles.html">Tile Box</a></li>
                            <li><a href="widgets-charts.html">Chart Widgets</a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-time"></i><span> Ride History </span> </a>

                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-comment-more"></i><span> Reviews & Comments </span> </a>

                    </li>

                    <li class="has_sub">
                        <a href="<?php echo base_url('admin/promo_codes')?>" class="waves-effect"><i class="zmdi zmdi-local-activity"></i> <span> Promo Codes </span> </a>

                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-key"></i><span> Access Control </span></a>

                    </li>

                    <li class="text-muted menu-title">More</li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-settings"></i><span> Settings </span><span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="pages-starter.html">Starter Page</a></li>
                            <li><a href="pages-login.html">Login</a></li>
                            <li><a href="pages-register.html">Register</a></li>
                            <li><a href="pages-recoverpw.html">Recover Password</a></li>
                            <li><a href="pages-lock-screen.html">Lock Screen</a></li>
                            <li><a href="pages-404.html">Error 404</a></li>
                            <li><a href="pages-500.html">Error 500</a></li>
                            <li><a href="pages-timeline.html">Timeline</a></li>
                            <li><a href="pages-invoice.html">Invoice</a></li>
                            <li><a href="pages-pricing.html">Pricing</a></li>
                            <li><a href="pages-gallery.html">Gallery</a></li>
                            <li><a href="pages-maintenance.html">Maintenance</a></li>
                            <li><a href="pages-comingsoon.html">Coming Soon</a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-power"></i></i><span>Log Out</span> </a>

                    </li>

                </ul>
                <div class="clearfix"></div>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

        </div>

    </div>
    <!-- Left Sidebar End -->
<script type="text/javascript">
    $("#search_category").on("change",function(){
        var _cat = $("#search_category").val();
        if(_cat == 'bikes'){
            $("#search-user").attr('placeholder','Search Bikes')
        }else{
            $("#search-user").attr('placeholder','Search Riders')
        }
    })
</script>