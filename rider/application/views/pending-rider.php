<style type="text/css">
@media screen and (min-width: 100px) {
    .modal-dialog {
        left: -15%;
        right: auto;
        /*width: 624px;*/
    }
}
</style>
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">


            <!-- end row -->


            <div class="row">
                <div class="col-12">
                    <div class="card-box no-padding">
                        <div class="row">

                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12 m-t-sm-40 no-padding">
                                    <div class="card-box-padding">
                                        <div class="row">
                                            <div class="col-4">
                                                <fieldset>
                                                    <div class="form-group">
                                                        <input type="hidden" name="hddrID" value="<?php echo $location->rID;?>">
                                                        <label for="disabledTextInput">First Name</label>
                                                        <p><?php echo @$location->rName;?></p>
                                                    </div>

                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput3">Contact</label>
                                                    <p><?php echo @$location->rPhone;?></p>
                                                    
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput">External Rider</label><br>
                                                    <p><?php echo @$location->rInExRiders;?></p>
                                                </fieldset>
                                            </div>

                                           <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">Last Name</label>
                                                    <p><?php echo @$location->rLastName;?></p>
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput2">Email</label>
                                                    <p><?php echo @$location->rEmail;?></p>
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="exampleSelect1">Assign Bike</label>
                                                    <p><?php echo @$location->rAssignBike;?></p>
                                                </fieldset>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="divider"></div>


                                    <div class="card-box-padding">
                                        <div class="row">
                                            <div class="col-4">
                                                <fieldset>
                                                    <div class="form-group">
                                                        <label for="disabledTextInput">EPF Number</label>
                                                        <p><?php echo @$location->rEpfNumber;?></p>
                                                    </div>

                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput3">Salary</label>
                                                    <p><?php echo @$location->rSalary;?></p>
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput3">Phone EMI Number</label>
                                                    <p><?php echo @$location->rEmiNumber;?></p>
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">Equipments</label>
                                                    <p><?php echo @$location->rEquipments;?></p>
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput2">Initial Deposite</label>
                                                    <p><?php echo @$location->rInitialDeposite;?></p>
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <p><?php echo @$location->rBykerPhoneNumber;?></p>
                                                    <label for="TextInput4">Byker Phone Number</label>
                                                </fieldset>

                                            </div>

                                        </div>

                                    </div>
                                    <div class="divider"></div>


                                    <div class="card-box-padding">
                                        <div class="row">

                                            <div class="col-12">
                                                <h4 class="header-title m-t-0 m-b-30">Documents</h4>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-4">
                                                <fieldset>
                                                    <?php $ncf = $this->admin_model->GetInfoByRow('rider_document','',array('rID' => $location->rID, 'subID' => 1));
                                                        if(@$ncf->rdStatus == 3){
                                                            $ncf_stat = '<i style="color:#00FA9A; font-size:18px" class="zmdi zmdi-check-circle"></i>';
                                                        }else{
                                                            $ncf_stat = '<i style="color:#CD5C5C; font-size:18px" class="zmdi zmdi-close-circle"></i>';
                                                        }
                                                    ?>
                                                    <div class="form-group">
                                                        <label for="disabledTextInput"><?php echo $ncf_stat?> National Identity Card (Front)</label>
                                                        <input type="file" name="image[]" id="input-file-now-disabled-2" class="dropify external" data-default-file="<?php echo base_url('uploads/document_img/'.@$ncf->rdImage)?>" data-toggle="modal" data-target=".bs-example-modal-lg" data-label="National Identity Card (Front)" data-id = "<?php echo @$ncf->rdID?>">
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <?php $ncb = $this->admin_model->GetInfoByRow('rider_document','',array('rID' => $location->rID, 'subID' => 2));
                                                        if(@$ncb->rdStatus == 3){
                                                            $ncb_stat = '<i style="color:#00FA9A; font-size:18px" class="zmdi zmdi-check-circle"></i>';
                                                        }else{
                                                            $ncb_stat = '<i style="color:#CD5C5C; font-size:18px" class="zmdi zmdi-close-circle"></i>';
                                                        }
                                                    ?>
                                                    <label for="TextInput3"><?php echo $ncb_stat; ?> National Identity Card (Back)</label>
                                                    <input type="file" name="image[]" data-default-file="<?php echo base_url('uploads/document_img/'.@$ncb->rdImage)?>" id="input-file-now-disabled-2" class="dropify external" data-height="172" data-toggle="modal" data-target=".bs-example-modal-lg" data-label="National Identity Card (Back)" data-id = "<?php echo @$ncb->rdID?>"/>

                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <?php $bpb = $this->admin_model->GetInfoByRow('rider_document','',array('rID' => $location->rID, 'subID' => 3));
                                                        if(@$bpb->rdStatus == 3){
                                                            $bpb_stat = '<i style="color:#00FA9A; font-size:18px" class="zmdi zmdi-check-circle"></i>';
                                                        }else{
                                                            $bpb_stat = '<i style="color:#CD5C5C; font-size:18px" class="zmdi zmdi-close-circle"></i>';
                                                        }
                                                    ?>
                                                    <label for="TextInput3"><?php echo $bpb_stat ?> Bank Pass Book</label>
                                                    <input type="file" name="image[]" data-default-file="<?php echo base_url('uploads/document_img/'.@$bpb->rdImage)?>"  id="input-file-now-disabled-2" class="dropify external" data-height="172" data-toggle="modal" data-target=".bs-example-modal-lg" data-label="Bank Pass Book" data-id = "<?php echo @$bpb->rdID?>"/>

                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <?php $dlf = $this->admin_model->GetInfoByRow('rider_document','',array('rID' => $location->rID, 'subID' => 4));
                                                        if(@$dlf->rdStatus == 3){
                                                            $dlf_stat = '<i style="color:#00FA9A; font-size:18px" class="zmdi zmdi-check-circle"></i>';
                                                        }else{
                                                            $dlf_stat = '<i style="color:#CD5C5C; font-size:18px" class="zmdi zmdi-close-circle"></i>';
                                                        }
                                                    ?>
                                                    <label for="TextInput4"><?php echo $dlf_stat ?> Driving License (Front)</label>
                                                    <input type="file" name="image[]" data-default-file="<?php echo base_url('uploads/document_img/'.@$dlf->rdImage)?>" id="input-file-now-disabled-2" class="dropify external" data-height="172" data-toggle="modal" data-target=".bs-example-modal-lg" data-label="Driving License (Front)" data-id = "<?php echo @$dlf->rdID?>"/>

                                                </fieldset>
                                                <fieldset>
                                                    <label for="TextInput4">Insurance Expiry</label>
                                                    <div class="input-group">

                                                        <input type="text" class="form-control datepicker" placeholder="mm/dd/yyyy" name="driving_expe[3]" id="datepicker-autoclose" value="<?php echo @$dlf->rdExDate ?>">
                                                        <span class="input-group-addon bg-custom b-0"><i class="icon-calender"></i></span>
                                                    </div><!-- input-group -->
                                                </fieldset>

                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <?php $dlb = $this->admin_model->GetInfoByRow('rider_document','',array('rID' => $location->rID, 'subID' => 5));
                                                        if(@$dlb->rdStatus == 3){
                                                            $dlb_stat = '<i style="color:#00FA9A; font-size:18px" class="zmdi zmdi-check-circle"></i>';
                                                        }else{
                                                            $dlb_stat = '<i style="color:#CD5C5C; font-size:18px" class="zmdi zmdi-close-circle"></i>';
                                                        }
                                                    ?>
                                                    <label for="TextInput2"><?php echo $dlb_stat?> Driving License (Back)</label>
                                                    <input type="file" name="image[]" id="input-file-now-disabled-2" data-default-file="<?php echo base_url('uploads/document_img/'.@$dlb->rdImage)?>" class="dropify external" data-height="172" data-toggle="modal" data-target=".bs-example-modal-lg" data-label="Driving License (Back)" data-id = "<?php echo @$dlb->rdID?>"/>
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <?php $rpd = $this->admin_model->GetInfoByRow('rider_document','',array('rID' => $location->rID, 'subID' => 6));
                                                        if(@$rpd->rdStatus == 3){
                                                            $rpd_stat = '<i style="color:#00FA9A; font-size:18px" class="zmdi zmdi-check-circle"></i>';
                                                        }else{
                                                            $rpd_stat = '<i style="color:#CD5C5C; font-size:18px" class="zmdi zmdi-close-circle"></i>';
                                                        }
                                                    ?>
                                                    <label for="TextInput4"><?php echo $rpd_stat?> Revenue License</label>
                                                    <input type="file" name="image[]" id="input-file-now-disabled-2" data-default-file="<?php echo base_url('uploads/document_img/'.@$rpd->rdImage)?>" class="dropify external" data-height="172" data-toggle="modal" data-target=".bs-example-modal-lg" data-label="Revenue License" data-id = "<?php echo @$rpd->rdID?>"/>
                                                </fieldset>
                                                <fieldset>
                                                    <label for="TextInput4">Revenue Expiry</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control datepicker" placeholder="mm/dd/yyyy" name="driving_expe[5]" value="<?php echo @$rpd->rdExDate?>" id="datepicker-autoclose" >
                                                        <span class="input-group-addon bg-custom b-0"><i class="icon-calender"></i></span>
                                                    </div><!-- input-group -->
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <?php $pr = $this->admin_model->GetInfoByRow('rider_document','',array('rID' => $location->rID, 'subID' => 7));
                                                     if(@$pr->rdStatus == 3){
                                                            $pr_stat = '<i style="color:#00FA9A; font-size:18px" class="zmdi zmdi-check-circle"></i>';
                                                        }else{
                                                            $pr_stat = '<i style="color:#CD5C5C; font-size:18px" class="zmdi zmdi-close-circle"></i>';
                                                        }
                                                    ?>
                                                    <label for="TextInput4"><?php echo $pr_stat ?> Police Report / Grama Niladari Report (Optional)</label>
                                                    <input type="file" name="image[]" id="input-file-now-disabled-2" data-default-file="<?php echo base_url('uploads/document_img/'.@$pr->rdImage)?>" class="dropify external" data-height="172" data-toggle="modal" data-target=".bs-example-modal-lg" data-label="Police Report / Grama Niladari Report (Optional)" data-id = "<?php echo @$pr->rdID?>"/>

                                                </fieldset>

                                            </div>
                                        </div>
                                        <div class="form-group text-right m-b-0">
                                            <button class="btn btn-cancel  waves-effect waves-light" type="reset">
                                                Cancel
                                            </button>

                                            <?php
                                            $query1 = "SELECT rID as rdoc_ID, count(rdID) as no_of_doc FROM `rider_document` WHERE rID = $location->rID AND rdStatus=3 group by rID";
                                            $chckdoc = $this->admin_model->getDetailQuery($query1);
                                            $button_stat = '';
                                            if(@$chckdoc[0]['no_of_doc'] <> 7){
                                                $button_stat = 'disabled';
                                            }
                                            ?>
                                            <button id="add-loc-btn" class="btn  btn-send btn btn-save waves-effect m-l-5 <?php echo $button_stat?>" type="submit">Save</button>
                                            <!--                                        <button type="reset" class="btn btn-save waves-effect m-l-5">-->
                                            <!--                                            Save-->
                                            <!--                                        </button>-->
                                        </div>
                                    </div>
                            </div><!-- end col -->

                        </div><!-- end row -->
                    </div>
                </div><!-- end col -->
            </div>
            <!-- end row -->



        </div> <!-- container -->

    </div> <!-- content -->

     <!-- Modal -->

<form autocomplete="off" method="post" action="<?php echo  base_url() ?>admin/approve_reject_process">
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content"  style="height: 700px; width: 1500px " >
            <div class="modal-header">
                <h5 class="modal-title" id="myLargeModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-4" id="content-display"></div>
                  <div class="col-md-4 ml-auto">
                       <fieldset>
                            <label for="TextInput4" id="forExpiration">Expiration</label>
                            <div class="input-group">
                                <input type="text" class="form-control datepicker revenue_expire" placeholder="mm/dd/yyyy" name="expiration[5]" value="<?php echo @$rpd->rdExDate?>" id="datepicker-autoclose revenue_expire" >
                                <input type="text" class="form-control datepicker driving_expire" placeholder="mm/dd/yyyy" name="expiration[5]" value="<?php echo @$dlf->rdExDate?>" id="datepicker-autoclose driving_expire" >
                                <!-- <span class="input-group-addon bg-custom b-0"><i class="icon-calender"></i></span> -->
                            </div><!-- input-group -->
                        </fieldset>
                        </br>
                        <fieldset>
                            <label for="TextInput4">Add a reason if you want to reject</label>
                            <div class="input-group">
                                <textarea name="remarks" class="form-control" rows="6"></textarea>
                            </div>
                        </fieldset>
                        <br/>
                         <fieldset>
                            <input type="hidden" name="hddrdID" id="hddrdID">
                            <input type="hidden" name="hddrID" value="<?php echo $location->rID?>">
                            <input type="submit" name="btn_submit" value="Reject" class="btn btn-danger">
                            <input type="submit" name="btn_submit" value="Accept" class="btn btn-primary" >
                        </fieldset>
                  </div>
                </div>
                
              </div>
            </div>
         
        </div>
    </div>
    </div>
    </div>
</form>

</div>
<!-- End content-page -->



</div>
<!-- END wrapper -->

<script>
$('input.external').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('data-default-file');
        var _dataLabel = $(this).attr("data-label");
        var _dataID = $(this).attr("data-id");
        $("#hddrdID").val(_dataID);
        if(_dataLabel == "Revenue License"){
            // alert(_dataLabel);
            $("#forExpiration").show();
            $(".revenue_expire").show();
            $(".driving_expire").hide();
        }
        if(_dataLabel=="Driving License (Front)"){
            $("#forExpiration").show();
            $(".revenue_expire").hide();
            $(".driving_expire").show();
        }
        if(_dataLabel!="Driving License (Front)" && _dataLabel != "Revenue License"){
            $(".driving_expire").hide();
            $(".revenue_expire").hide();
            $("#forExpiration").hide();
        }
        $("#myLargeModalLabel").text(_dataLabel);
        $("#content-display").html('<iframe width="205%" height="610px" frameborder="10" scrolling="yes" allowtransparency="true" src="'+url+'" allowfullscreen></iframe>');
    }); 
    // function resizeIframe(obj) {
    //     obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
    //     // obj.style.width = obj.contentWindow.document.body.scrollWidth + 'px';
    //   }
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/detect.js"></script>
<script src="<?php echo base_url(); ?>assets/js/fastclick.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>

<!-- Autocomplete -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/autocomplete/jquery.mockjax.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/autocomplete/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/autocomplete/countries.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pages/jquery.autocomplete.init.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/pages/jquery.formadvanced.init.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/mjolnic-bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/clockpicker/bootstrap-clockpicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>



<!-- App js -->
<script src="<?php echo base_url(); ?>assets/js/jquery.core.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.app.js"></script>

<!-- file uploads js -->
<script src="<?php echo base_url(); ?>assets/plugins/fileuploads/js/dropify.min.js"></script>


<script>
$(".datepicker").datepicker({
autoclose: true,
todayHighlight: true,
format:'dd/mm/yyyy' 
});
    (function() {
        $(document).ready(function() {
            $('.switch-input').on('change', function() {
                var isChecked = $(this).is(':checked');
                var selectedData;
                var $switchLabel = $('.switch-label');
                console.log('isChecked: ' + isChecked);

                if(isChecked) {

                    $("#type").val("Internal");


                } else {
                    $("#type").val("External");
                }



            });

            // Params ($selector, boolean)
            function setSwitchState(el, flag) {
                el.attr('checked', flag);
            }

            // Usage
            setSwitchState($('.switch-input'), true);
        });

    })();


</script>
<script type="text/javascript">
    $('.dropify').dropify({
        messages: {
            'default': 'Drag and drop a file here or click',
            'replace': 'Drag and drop or click to replace',
            'remove': 'Remove',
            'error': 'Ooops, something wrong appended.'
        },
        error: {
            'fileSize': 'The file size is too big (1M max).'
        }
    });
</script>
<script type="text/javascript">
    function parkingFee() {
        if ($('#price').is(':checked')) {
            $('#price-details :input').attr('disabled', true);
        } else {
            $('#price-details :input').removeAttr('disabled');
        }
    }
</script>
<script>
    $('#add-tag').on('click', function (e) {
        if ($('#location-tags').val().trim() !== "") {
            var val = $('#location-tags').val();
            var tagHTML = '<div class="input-group tags-padding">' +
                '<input class="form-control" value="'+val+'" name="tags[]">' +
                '<span class="input-group-addon remove-tag"><i class="glyphicon glyphicon-remove"></i></span>' +
                '</div>';
            $('#show-tags').append(tagHTML);
            $('.remove-tag').on('click', function (e) {
                $(this).parent().remove();
            });
        }
        $('#location-tags').val('');
    });

</script>
<script>


    function removeImage(image,location){
        var id = "#remove-img-"+image;
        swal({
                title: "Are you sure?",
                text: "You want to Delete this?",
                type: "warning",   showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true },
            function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: "<?= base_url() ?>owner/deleteLocationImage",
                        type: 'POST',
                        data: {image: image,location:location},
                        success: function (res) {
                            if(res){
                                $(id).hide();
                                $('#show-img').html('');
                                $('#show-img').html(res);
                                swal("Success!", "Successfully Deleted ", "success");
                                $('.dropify').dropify({
                                    error: {
                                        'fileSize': 'The file size is too big ({{ value }} max).',
                                        'minWidth': 'The image width is too small ({{ value }}}px min).',
                                        'maxWidth': 'The image width is too big ({{ value }}}px max).',
                                        'minHeight': 'The image height is too small ({{ value }}}px min).',
                                        'maxHeight': 'The image height is too big ({{ value }}px max).',
                                        'imageFormat': 'The image format is not allowed ({{ value }} only).'
                                    }
                                });
                            }else{

                            }

                        }
                    });
                }
            });
    }

</script>
