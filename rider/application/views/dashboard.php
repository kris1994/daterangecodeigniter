
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="BykerTaxi">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">

    <!-- App title -->
    <title>Byker - Taxi</title>

    <!-- DataTables -->
    <link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="<?php echo base_url(); ?>assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Switchery css -->
    <link href="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.css" rel="stylesheet" />

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <script
        src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script
        src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.min.css">
    <!-- Switchery css -->
    <link href="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
    <!-- Plugins css-->
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script src="https://code.jquery.com/jquery-1.11.3.js"></script>

    <!-- form Uploads -->
    <link href="<?php echo base_url(); ?>assets/plugins/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/css/sweetalert.css" rel="stylesheet" type="text/css" />
    <!-- Plugins css -->
    <link href="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plugins/mjolnic-bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plugins/clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- C3 charts css -->
    <link href="<?php echo base_url(); ?>assets/plugins/c3/c3.min.css" rel="stylesheet" type="text/css"  />
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/morris/morris.css">

    <!-- Switchery css -->
    <link href="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.css" rel="stylesheet" />

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/simplePagination.css">
    <!-- App CSS -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- Modernizr js -->

    <script src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>
    <script>
        var base_url = "<?php echo base_url(); ?>";
    </script>
</head>


<body class="fixed-left body-main">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <a href="<?php echo base_url(); ?>admin/dashboard" class="logo">

                <span><img width="183" height="42" src="<?php echo base_url(); ?>assets/images/dash-logo@2x.png"></span></a>
            <a href="<?php echo base_url(); ?>admin/dashboard" class="logo">
                <span id="visible-engrage"><img width="43" height="42" src="<?php echo base_url(); ?>assets/images/bike@2x.png"></span></a>
        </div>

        <nav class="navbar-custom">

            <ul class="list-inline float-right mb-0">



                <li class="list-inline-item dropdown notification-list notification-border">
                    <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                       aria-haspopup="false" aria-expanded="false">
<!--                        <img src="--><?php //echo base_url(); ?><!--assets/images/users/user-image-menu.png" alt="user" class="rounded-circle">-->
                        <span class="admin_username hidden-xs"><?= $this->session->userdata['admin_username'] ?></span><i class="zmdi zmdi-chevron-down ico-deco"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                        <!-- item-->
                        <div class="dropdown-item noti-title">
                            <h5 class="text-overflow"><small>Welcome ! <?= $this->session->userdata['admin_username'] ?></small> </h5>
                        </div>

                        <!-- item-->
                        <a href="<?php echo base_url(); ?>change-password" class="dropdown-item notify-item">
                            <i class="zmdi zmdi-account-circle"></i> <span>Change Password</span>
                        </a>


                        <a href="<?= base_url() ?>admin/logout" class="dropdown-item notify-item">
                            <i class="zmdi zmdi-power"></i> <span>Logout</span>
                        </a>

                    </div>
                </li>
                <li class="list-inline-item dropdown notification-list ">
                    <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown" href="#" role="button"
                       aria-haspopup="false" aria-expanded="false">
                        <i class="zmdi zmdi-notifications noti-icon"></i>
                        <span class="noti-icon-badge"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-lg" aria-labelledby="Preview">
                        <!-- item-->
                        <div class="dropdown-item noti-title">
                            <h5><small><span class="label label-danger pull-xs-right">7</span>Notification</small></h5>
                        </div>

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>
                            <p class="notify-details">Robert S. Taylor commented on Admin<small class="text-muted">1min ago</small></p>
                        </a>

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <div class="notify-icon bg-info"><i class="icon-user"></i></div>
                            <p class="notify-details">New user registered.<small class="text-muted">1min ago</small></p>
                        </a>

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <div class="notify-icon bg-danger"><i class="icon-like"></i></div>
                            <p class="notify-details">Carlos Crouch liked <b>Admin</b><small class="text-muted">1min ago</small></p>
                        </a>

                        <!-- All-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item notify-all">
                            View All
                        </a>

                    </div>
                </li>
            </ul>

            <ul class="list-inline menu-left mb-0">
                <div class="container-fluid">
                    <div class="col-sm-4">
                        <h4 class="page-title float-left">Dashboard</h4>


                        <div class="clearfix"></div>
                    </div>
                </div>
            </ul>

        </nav>

    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <ul class="list-inline menu-left mb-30">
                    <li class="float-right">
                        <button class="button-menu-mobile open-left waves-light waves-effect">
                            <i class="ion-chevron-left"></i>
                        </button>
                    </li>

                </ul>
                <ul>
                    <li class="has_sub">
                        <a href="<?php echo base_url(); ?>admin/dashboard" class="waves-effect"><i class="zmdi zmdi-equalizer"></i><span> Dashboard </span> </a>
                    </li>
                    <li class="has_sub">
                        <a href="<?php echo base_url(); ?>admin/all-bikes/1" class="waves-effect"> <i class="zmdi zmdi-bike"></i><span> Bikes</span> </a>
                    </li>
                    <li class="has_sub">
                        <a href="#" class="waves-effect"> <img src="<?php echo base_url(); ?>assets/images/passenger-ic-dash@2x.png"><span> Passengers </span> </a>
                    </li>
                    <li class="has_sub">
                        <a href="<?php echo base_url(); ?>admin/all-riders/1" class="waves-effect"><i class="zmdi zmdi-account"></i><span> Riders </span> </a>
                    </li>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-local-activity"></i> <span> Promo Codes </span> </a>
                    </li>
                    <li class="text-muted menu-title">More</li>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-settings"></i><span> Settings </span></a>
                    </li>
                    <li class="has_sub">
                        <a href="<?= base_url() ?>admin/logout" class="waves-effect"><i class="zmdi zmdi-power"></i></i><span>Log Out</span> </a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

        </div>

    </div>
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-lg-12 col-xl-12">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <ul class="icons-list day-list">
                                    <li class="dropdown text-muted">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="week-base week-base-show-prt">Today</span><span class="caret"></span></a>
                                        <ul class="dropdown-menu dropdown-menu-right change-week-prt">
                                            <li><a href="javascript:void(0)" id="show-today-prt" >Today</a></li>
                                            <li><a href="javascript:void(0)" id="show-week-prt" >Week</a></li>
                                            <li><a href="javascript:void(0)" id="show-month-prt" >Month</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <h4 class="header-title m-t-0 m-b-20">Peek Ride Time</h4>
                            <!-- <div id="morris-bar-stacked" style="height: 320px;"></div> -->
                            <div id="peek-ride-time" style="height: 320px;"></div>
                        </div>
                    </div><!-- end col-->
                </div>
                <!-- end row -->

                <div class="row">
                        <div class="card-box tilebox-one" style="width: 15%">
                            <h6 class="title-card-text text-muted text-uppercase m-b-20">Rides This Week</h6>
                            <h2 class="m-b-20 counter-text-r" data-plugin=""><?php echo $getRideThisWeek?></h2>
                        </div>
                        <div class="card-box tilebox-one" style="width: 24%">
                            <div class="dropdown pull-right">
                                <ul class="icons-list day-list">
                                    <li class="dropdown text-muted">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="week-base week-base-show-re">Today</span><span class="caret"></span></a>
                                        <ul class="dropdown-menu dropdown-menu-right change-week-re">
                                            <li><a href="javascript:void(0)" id="show-today" >Today</a></li>
                                            <li><a href="javascript:void(0)" id="show-week" >Week</a></li>
                                            <li><a href="javascript:void(0)" id="show-month" >Month</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <h6 class="title-card-text text-muted text-uppercase m-b-20">Earnings This Week</h6>
                            <h2 class="m-b-20 counter-text-etw"><span data-plugin=""><?php echo $getEarningThisWeek?></span></h2>
                        </div>
                    <!-- </div> -->
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3  custom-col">

                        <div class="card-box tilebox-one">
                            <!-- <img class="float-right text-muted"  src="<?php echo base_url(); ?>assets/images/passender-ic.png"> -->
                            <h6 class="title-card-text text-muted text-uppercase m-b-20">Passengers Count<i class="pull-right zmdi zmdi-airline-seat-recline-normal"></i></h6>
                            <h2 class="m-b-20 counter-text" data-plugin="counterup"><?php echo $total; ?></h2>
                        </div>

                    </div>

                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3  custom-col">
                        <div class="card-box tilebox-one">
                            <!-- <img class="float-right text-muted"  src="<?php echo base_url(); ?>assets/images/ride-ic.png"> -->
                            <h6 class="title-card-text text-muted text-uppercase m-b-20"> Riders Count<i class="pull-right ion-ios7-person"></i></h6>
                            <h2 class="m-b-20 counter-text" data-plugin="counterup"><?php echo $total_rider; ?></h2>

                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3  custom-col">
                        <div class="card-box tilebox-one">
                            <!-- <img class="float-right text-muted"  src="<?php echo base_url(); ?>assets/images/ratings-ic.png"> -->
                            <h6 class="title-card-text text-muted text-uppercase m-b-20">Ratings <i class="pull-right ion-android-star"></i></h6>
                            <h2 class="m-b-20 counter-text" data-plugin="counterup">4.3</h2>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-lg-12 col-xl-12 m-t-20">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <ul class="icons-list day-list">
                                    <li class="dropdown text-muted">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="week-base week-base-show-earn"><?php echo date('Y')?></span><span class="caret"></span></a>
                                        <ul class="dropdown-menu dropdown-menu-right change-week-earn">
                                            <li><a href="javascript:void(0)" id="show-month" ><?php echo date('Y')?></a></li>
                                            <?php
                                            $months = array( 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' );
                                            foreach ($months as $m) {
                                            ?>
                                            <li><a href="javascript:void(0)" id="show-month" ><?php echo $m?></a></li>
                                            <?php }?>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <h4 class="header-title m-t-0">Earnings</h4>
                            <div class="p-20">
                                <div id="line-regions-x"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div><!-- end col-->
        </div>
        <!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->
</div>


</div>
<!-- END wrapper -->
<script>


// bar chart
$(function() {
  // Create a function that will handle AJAX requests
  function requestData(rideWeekStat, chart){
    $.ajax({
      type: "GET",
      dataType: 'json',
      url: "<?php echo site_url('admin/getRiderHistory') ?>", // This is the URL to the API
      data: { rideWeekStat: rideWeekStat }
    })
    .done(function( data ) {
      // When the response to the AJAX request comes back render the chart with new data
      chart.setData(data);
    })
    .fail(function() {
      // If there is no communication between the server, show an error
      alert( "error occured" );
    });
  }

  var chart = Morris.Bar({
    // ID of the element in which to draw the chart.
    element: 'peek-ride-time',
    data: [0, 0], // Set initial data (ideally you would provide an array of default data)
    xkey:'y',
    ykeys:['a'],
    labels:['Amount'],
    hideHover:'auto',
    barColors: ["#ff8000"],
    gridLineColor: ['#1bb99a'],
    stacked:true
  });

  // Request initial data for the past 7 rideWeekStat:

  requestData('Today', chart);

  $(".change-week-prt").on("click","li a",function(e){
      e.preventDefault();
        $(".week-base-show-prt").text($(this).text());
        rideWeekStat = $(this).text();
        requestData(rideWeekStat, chart);
  });

  // Earnings -------------------------------------------

    function requestDataEarn(earnMonthStat, chartE){
        $.ajax({
              type: "GET",
              dataType: 'json',
              url: "<?php echo site_url('admin/getEarningPerMonth') ?>", // This is the URL to the API
              data: { earnMonthStat: earnMonthStat }
            })
            .done(function( data ) {
              // When the response to the AJAX request comes back render the chartE with new data
              chartE.setData(data);
            })
            .fail(function() {
              // If there is no communication between the server, show an error
              alert( "error occured" );
            });
    }
  
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var chartE = Morris.Line({
        element: 'line-regions-x', 
        behaveLikeLine: true,
        parseTime : false, 
        data: [0, 0], // Set initial data (ideally you would provide an array of default data)
        xkey: 'm',
        ykeys: ['a'],
        labels: ['Earning'],
        pointFillColors: ['#707f9b'],
        pointStrokeColors: ['#ffaaab'],
        lineColors: ['#f26c4f'],
        redraw: true
     });
    
    var currentTime = new Date()
    // returns the year (four digits)
    var year = currentTime.getFullYear()
    requestDataEarn(year, chartE);

    $(".change-week-earn").on("click","li a",function(e){
        $('.week-base-show-earn').text($(this).text());
        earnMonthStat = $(this).text();
        requestDataEarn(earnMonthStat, chartE);
    });
});

</script>
<script>
    $(".change-week-re").on("click","li a",function(){
        $(".week-base-show-re").text($(this).text());
         $.ajax({
            type: "POST",
            dataType: 'json',
            url: "<?php echo base_url(); ?>" + "admin/getRiderEarnings",
            data: { REweekStat: $(this).text()},
            success: function (res) {
                $(".counter-text-r").text(res[0]['totalRE']);
                $(".counter-text-etw").text(res[0]['totalEarning']);
            }
        });
    })
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/detect.js"></script>
<script src="<?php echo base_url(); ?>assets/js/fastclick.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.js"></script>

<!--Morris Chart-->
<script src="<?php echo base_url(); ?>assets/plugins/morris/morris.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/raphael/raphael-min.js"></script>


<!--C3 Chart-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/d3/d3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/c3/c3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pages/jquery.c3-chart.init.js"></script>


<!-- Counter Up  -->
<script src="<?php echo base_url(); ?>assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/counterup/jquery.counterup.min.js"></script>

<!-- App js -->
<script src="<?php echo base_url(); ?>assets/js/jquery.core.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.app.js"></script>

<!-- Page specific js -->
<script src="<?php echo base_url(); ?>assets/pages/jquery.dashboard.js"></script>

</body>
</html>