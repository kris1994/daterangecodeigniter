<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">

    <!-- App title -->
    <title>BykerTaxi - Dashboard </title>

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- App CSS -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Oxygen:300,400,700" rel="stylesheet">
    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>

</head>


<body>

<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="row no-gutters">
    <div class="col-6 col-md-4"></div>
    <div class="col-12 col-sm-6 col-md-8">
        <div class="wrapper-page">

            <div class="account-bg">
                <div class="mb-0">
                    <div class="text-center m-t-20">
                        <a href="index.html" class="logo">
                            <span><img src="<?php echo base_url(); ?>assets/images/logo.png"></span>
                        </a>
                    </div>
                    <div class="m-t-10 p-20">

                                    <?php if ($this->session->flashdata('errors')): ?>
                                        <div class="display-message-text text-center">
                                            <?php echo $this->session->flashdata('errors'); ?>
                                        </div>
                                    <?php endif; ?>

                                    <form class="m-t-20"  action="<?php echo base_url()?>Admin/checkLogin" method="post">
                                        <div class="form-group has-feedback col-12 text-center">
                                            <label>Email address</label>
                                            <input type="email" name="email" class="form-control form-control-login"  placeholder="Enter email">
                                        </div>
                                        <div class="form-group has-feedback col-12 text-center">
                                            <label>Password</label>
                                            <input type="password" name="password" class="form-control form-control-login" placeholder="Password">
                                        </div>
<!--                                        <div class="form-group row">-->
<!--                                            <div class="col-12 text-center">-->
<!--                                                <div class="checkbox checkbox-circle">-->
<!--                                                    <input id="checkbox-signup" type="checkbox">-->
<!--                                                    <label for="checkbox-signup">-->
<!--                                                        Remember me-->
<!--                                                    </label>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
                                        <div class="form-group text-center row m-t-30">
                                            <div class="col-12">
                                                <button class="btn btn-login btn-success btn-block waves-effect waves-light" type="submit">Log In</button>
                                            </div>
                                        </div>

<!--                                        <div class="form-group row m-t-30 mb-0">-->
<!--                                            <div class="col-12 text-center">-->
<!--                                                <a href="pages-recoverpw.html" class="text-muted text-forget"> Forgot your password?</a>-->
<!--                                            </div>-->
<!--                                        </div>-->
                                    </form>

                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- end card-box-->

            <div class="m-t-20">
                <div class="text-center">
                    <p class="text-white">Don't have an account? <a href="pages-register.html" class="text-white m-l-5"><b>Sign Up</b></a></p>
                </div>
            </div>

        </div></div>

</div>
<!-- end wrapper page -->


<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/detect.js"></script>
<script src="<?php echo base_url(); ?>assets/js/fastclick.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.js"></script>

<!-- App js -->
<script src="<?php echo base_url(); ?>assets/js/jquery.core.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.app.js"></script>

</body>
</html>
