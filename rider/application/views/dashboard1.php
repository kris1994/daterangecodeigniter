
    <script type="text/javascript">
        $(document).ready(function(){
            $.getJSON("<?php echo site_url('admin/getRiderHistory'); ?>", function (json) { 
                var acctregs = new Morris.Bar({
                            element: 'peek-ride-time',
                            data: json,
                            xkey:'y',
                            ykeys:['a'],
                            labels:['Amount'],
                            hideHover:'auto',
                            barColors: ["#1bb99a"],
                            gridLineColor: ['#1bb99a'],
                            stacked:true
                        });
            });
        });
    </script>
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-lg-12 col-xl-12">
                        <div class="card-box">

                            <h4 class="header-title m-t-0 m-b-20">Peek Ride Time</h4>
                            <!-- <div id="morris-bar-stacked" style="height: 320px;"></div> -->
                            <div id="peek-ride-time" style="height: 320px;"></div>

                        </div>
                    </div><!-- end col-->

                </div>
                <!-- end row -->

                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3 custom-col">
                        <div class="card-box tilebox-one">
                            <img class="float-right text-muted" src="<?php echo base_url(); ?>assets/images/ride-ic.png">
                            <h6 class="title-card-text text-muted text-uppercase m-b-20">Rides This Week</h6>
                            <h2 class="m-b-20 counter-text" data-plugin=""><?php echo $getRideThisWeek?></h2>

                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3  custom-col">
                        <div class="card-box tilebox-one">
                            <img class="float-right text-muted"  src="<?php echo base_url(); ?>assets/images/earning-ic.png">
                            <h6 class="title-card-text text-muted text-uppercase m-b-20">Earnings This Week</h6>
                            <h2 class="m-b-20 counter-text">$<span data-plugin=""><?php echo $getEarningThisWeek?></span></h2>

                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3  custom-col">
                        <div class="card-box tilebox-one">
                            <img class="float-right text-muted"  src="<?php echo base_url(); ?>assets/images/passender-ic.png">
                            <h6 class="title-card-text text-muted text-uppercase m-b-20">Passengers Count</h6>
                            <h2 class="m-b-20 counter-text" data-plugin="counterup"><?php echo $total; ?></h2>

                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3  custom-col">
                        <div class="card-box tilebox-one">
                            <img class="float-right text-muted"  src="<?php echo base_url(); ?>assets/images/ride-ic.png">
                            <h6 class="title-card-text text-muted text-uppercase m-b-20"> Riders Count</h6>
                            <h2 class="m-b-20 counter-text" data-plugin="counterup"><?php echo $total_rider; ?></h2>

                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3  custom-col">
                        <div class="card-box tilebox-one">
                            <img class="float-right text-muted"  src="<?php echo base_url(); ?>assets/images/ratings-ic.png">
                            <h6 class="title-card-text text-muted text-uppercase m-b-20">Ratings</h6>
                            <h2 class="m-b-20 counter-text" data-plugin="counterup">4.3</h2>

                        </div>
                    </div>
                </div>



                <!-- end row -->




                <div class="row">
                    <div class="col-xs-12 col-lg-12 col-xl-12 m-t-20">
                        <div class="card-box">

                            <h4 class="header-title m-t-0">Earnings</h4>


                            <div class="p-20">
                                <div id="line-regions"></div>
                            </div>
                        </div>

                    </div>

                </div>
            </div><!-- end col-->
        </div>
        <!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->

</div>
<!-- End content-page -->
</div>
<!-- END wrapper -->


<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/detect.js"></script>
<script src="<?php echo base_url(); ?>assets/js/fastclick.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.js"></script>

<!--Morris Chart-->
<script src="<?php echo base_url(); ?>assets/plugins/morris/morris.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/raphael/raphael-min.js"></script>


<!--C3 Chart-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/d3/d3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/c3/c3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pages/jquery.c3-chart.init.js"></script>


<!-- Counter Up  -->
<script src="<?php echo base_url(); ?>assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/counterup/jquery.counterup.min.js"></script>

<!-- App js -->
<script src="<?php echo base_url(); ?>assets/js/jquery.core.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.app.js"></script>

<!-- Page specific js -->
<script src="<?php echo base_url(); ?>assets/pages/jquery.dashboard.js"></script>

</body>
</html>