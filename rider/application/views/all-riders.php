
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <?php
            $segment = $this->uri->segment(3);
            if($segment == null) {
                $table_num_rows = 0;
                $showing = 1;
            } else if($segment == 1) {
                $table_num_rows = 0;
                $showing = 1;
            }elseif($segment > 1){
                $pgs= $segment - 1;
                $table_num_rows = 10 * $pgs;
                $showing = 10 * $pgs;
            }
            ?>
            <!-- end row -->


            <section class="content-header">
                <ul class="list-inline menu-left mb-0">
                    <form autocomplete="off" id="search-all-form" method="post" action="<?= base_url() ?>admin/search-user/1">
                    <li class="hidden-mobile app-search">
                        <div class="input-group custom-input-group">

                            <input type="hidden" name="search_param" value="name" id="search_param">
                            <input name="search" autocomplete="off" id="search-user" class="form-control" placeholder="Search User">
<!--                            <input type="text" class="form-control" name="q" placeholder="Search.." id="search_key" value="">-->

                            <button type="submit" class="button-insider-search"><i class="fa fa-search"></i></button>
                        </div>
                    </li>
                    </form>
                </ul>
                <ol class="breadcrumb">
                    <div class="col-sm-8">




                <form id="sort-submits" method="post" action="<?= base_url() ?>admin/sortLocation/1">
                    <div class="input-group input-daterange ">
                        <span class="input-group-addon daterange-div text-sm-dt">Date Joined</span> <input id="startDate1" name="startDate1" type="text"
                               class="form-control dateranger" readonly="readonly" placeholder="From"> <span
                            class="input-group-addon"> <span
                                class="zmdi zmdi-calendar color-change"></span>
		</span>  <input id="endDate1"
                                                                 name="endDate1" type="text" class="form-control dateranger" readonly="readonly" placeholder="To">
                        <span class="input-group-addon daterange-span-radius"> <span
                                class="zmdi zmdi-calendar color-change"></span>
		</span>
                    </div>

                </form>
                    </div>
                </ol>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url()?>admin/addRider"><button type="button" class="btn btn-addnew " ><!--<img class="btn-bike-img"  src="<?php echo base_url(); ?>assets/images/btn-bike.png">--><i class="zmdi zmdi-bike"></i>Add New</button></a></li>
                </ol>
            </section>

<!--            <form autocomplete="off" id="search-all-form" method="post" action="--><?//= base_url() ?><!--admin/search-user/1">-->
<!--                <div class="col-sm-10">-->
<!--                    <input name="search" autocomplete="off" id="search-user" class="form-control" placeholder="Search User">-->
<!--                </div>-->
<!--                <div class="col-sm-2">-->
<!--                    <button type="submit" class="btn btn-info pull-right">Search</button>-->
<!--                </div>-->
<!--                <div class="search-result-user" id="searchResult">-->
<!---->
<!--                </div>-->
<!--            </form>-->
<!--            <form id="sort-submits" method="post" action="--><?//= base_url() ?><!--admin/sort-user/1">-->
<!--                <div class="col-sm-4 form-group">-->
<!--                    <div class="col-sm-12">-->
<!--                        <input autocomplete="off" id="sort-user" value="" name="option" class="datepicker form-control" placeholder="2017-01-10">-->
<!--                    </div>-->
<!--                </div>-->
<!--            </form>-->
            <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive table-margin">
                        <!--<h4 class="m-t-0 header-title"><b>Default Example</b></h4>-->
                        <!--<p class="text-muted font-13 m-b-30">-->
                        <!--DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.-->
                        <!--</p>-->

                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr class="div-bottom-line">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Contact Number</th>
                                <th>Joined Date</th>
                                <th>Rating</th>
                                <th>Type</th>
                                <th class="display-none-div action-th">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php if (count($all_riders) != 0): ?>
                                <?php foreach ($all_riders as $value): ?>

                                    <tr>
                                        <td>
                                            <?php echo $value['rID'] ?>
                                        </td>

                                        <td>
                                            <?php
                                            if($value['rName']){
                                                echo $value['rName'];
                                            }else{
                                                echo '-';
                                            }?>
                                        </td>
                                        <td>
                                            <?php
                                            if($value['rPhone']){
                                                echo $value['rPhone'];
                                            }else{
                                                echo '-';
                                            }?>
                                        </td>
                                        <td>
                                            <?php
                                            if($value['rJoinDate']){
                                                echo $value['rJoinDate'];
                                            }else{
                                                echo '-';
                                            }?>
                                        </td>
                                        <td>
                                            <?php
                                            if($value['rRatings']){
                                                echo $value['rRatings'];
                                            }else{
                                                echo '-';
                                            }?>
                                        </td>
                                        <td>
                                            <?php
                                            if($value['rInExRiders']){
                                                echo $value['rInExRiders'];
                                            }else{
                                                echo '-';
                                            }?>
                                        </td>
                                        <td class="tbl-action">
                                            <a href="<?php echo base_url(); ?>admin/edit-rider/<?php echo $value['rID'];?>"><i class="zmdi zmdi-edit"></i></a>
                                        </td>

                                    </tr>
                                    <?php $table_num_rows++; ?>
                                <?php endforeach ?>
                                <?php
                                $detail_show = false;
                                ?>
                            <?php else: ?>
                                <?php
                                $detail_show = true;
                                ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <div class="row">
                            <!--                            <div class="col-md-3">-->
                            <!--                                --><?php
                            //
                            //                                if(!$detail_show) {
                            //                                    echo "Showing $showing to $table_num_rows of $total_db_rows entries";
                            //                                }
                            //                                ?>
                        </div>
                        <!--                            <div class="col-md-9">-->
                        <!--                                <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">-->
                        <!--                                    --><?php //echo $pagination_links; ?>
                        <!--                                </div>-->
                        <!--                            </div>-->
                    </div>
                </div>
            </div>
        </div> <!-- end row -->



        <!-- end row -->


    </div> <!-- container -->

</div> <!-- content -->



</div>
<!-- End content-page -->


<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->


<!-- Right Sidebar -->
<div class="side-bar right-bar">
    <div class="nicescroll">
        <ul class="nav nav-pills nav-justified text-xs-center">
            <li class="nav-item">
                <a href="#home-2"  class="nav-link active" data-toggle="tab" aria-expanded="false">
                    Activity
                </a>
            </li>
            <li class="nav-item">
                <a href="#messages-2" class="nav-link" data-toggle="tab" aria-expanded="true">
                    Settings
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade active show" id="home-2">
                <div class="timeline-2">
                    <div class="time-item">
                        <div class="item-info">
                            <small class="text-muted">5 minutes ago</small>
                            <p><strong><a href="#" class="text-info">John Doe</a></strong> Uploaded a photo <strong>"DSC000586.jpg"</strong></p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <small class="text-muted">30 minutes ago</small>
                            <p><a href="" class="text-info">Lorem</a> commented your post.</p>
                            <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <small class="text-muted">59 minutes ago</small>
                            <p><a href="" class="text-info">Jessi</a> attended a meeting with<a href="#" class="text-success">John Doe</a>.</p>
                            <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <small class="text-muted">1 hour ago</small>
                            <p><strong><a href="#" class="text-info">John Doe</a></strong>Uploaded 2 new photos</p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <small class="text-muted">3 hours ago</small>
                            <p><a href="" class="text-info">Lorem</a> commented your post.</p>
                            <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <small class="text-muted">5 hours ago</small>
                            <p><a href="" class="text-info">Jessi</a> attended a meeting with<a href="#" class="text-success">John Doe</a>.</p>
                            <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="messages-2">

                <div class="row m-t-10">
                    <div class="col-8">
                        <h5 class="m-0">Notifications</h5>
                        <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                    </div>
                    <div class="col-4 text-right">
                        <input type="checkbox" checked data-plugin="switchery" data-color="#1bb99a" data-size="small"/>
                    </div>
                </div>

                <div class="row m-t-10">
                    <div class="col-8">
                        <h5 class="m-0">API Access</h5>
                        <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                    </div>
                    <div class="col-4 text-right">
                        <input type="checkbox" checked data-plugin="switchery" data-color="#1bb99a" data-size="small"/>
                    </div>
                </div>

                <div class="row m-t-10">
                    <div class="col-8">
                        <h5 class="m-0">Auto Updates</h5>
                        <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                    </div>
                    <div class="col-4 text-right">
                        <input type="checkbox" checked data-plugin="switchery" data-color="#1bb99a" data-size="small"/>
                    </div>
                </div>

                <div class="row m-t-10">
                    <div class="col-8">
                        <h5 class="m-0">Online Status</h5>
                        <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                    </div>
                    <div class="col-4 text-right">
                        <input type="checkbox" checked data-plugin="switchery" data-color="#1bb99a" data-size="small"/>
                    </div>
                </div>

            </div>
        </div>
    </div> <!-- end nicescroll -->
</div>
<!-- /Right-bar -->




</div>
<!-- END wrapper -->
<?php if($total_db_rows >10){ ?>
    <script>
        $(function() {
            $('#example2_paginate').pagination({
                items: <?= $total_db_rows ?>,
                itemsOnPage: 10,
                cssStyle: 'compact-theme',
                hrefTextPrefix: '',
                displayedPages: 3

            });
            $('#example2_paginate').pagination('drawPage', <?= $segment ?>);
        });
    </script>
<?php } ?>
<!-- END wrapper -->
<?php if($total_db_rows >10){ ?>
    <script>
        $(function() {
            $('#example2_paginate').pagination({
                items: <?= $total_db_rows ?>,
                itemsOnPage: 10,
                cssStyle: 'compact-theme',
                hrefTextPrefix: '',
                displayedPages: 3

            });
            $('#example2_paginate').pagination('drawPage', <?= $segment ?>);
        });
    </script>
<?php } ?>


<script>
    $(document).ready(function () {
        $('.datepicker').datetimepicker({
            endDate: new Date(),
            weekStart: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: 'yyyy-mm-dd'
        });
        $('#sort-user').on('change', function(){
            var val = $(this).val();
            $('form#sort-submits').submit();
        });
        $('#sort-by-active').on('change', function(){
            var val = $(this).val();
            $('form#sort-submit-active').submit();
        });


        $("#search-user").on('keyup', function () {
            var user = $('#search-user').val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "admin/searchUserAutoComplete",
                data: {user: user},
                success: function (res) {
                    $("#searchResult").html(res);
                    $("#searchResult").slideDown();
                }
            });
        });
        $('#searchResult').on('click', '.search-text',function () {
            var searchKey = $(this).attr("data-value");
            $("#search-user").val(searchKey);
            $('form#search-all-form').trigger('submit');

        });

    });
</script>


<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/detect.js"></script>
<script src="<?php echo base_url(); ?>assets/js/fastclick.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.js"></script>

<!-- Required datatable js -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/buttons.colVis.min.js"></script>
<!-- Responsive examples -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/responsive.bootstrap4.min.js"></script>

<!-- App js -->
<script src="<?php echo base_url(); ?>assets/js/jquery.core.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.app.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();

        //Buttons examples
        var table = $('#datatable-buttons').DataTable({
            lengthChange: false,
            buttons: ['copy', 'excel', 'pdf', 'colvis']
        });

        table.buttons().container()
            .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
    } );

</script>




<script
    src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.input-daterange input').each(function() {
            $(this).datepicker();
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('.datepicker').datetimepicker({
            endDate: new Date(),
            weekStart: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: 'yyyy-mm-dd'
        });
        $('#sort-user').on('change', function(){
            var val = $(this).val();
            $('form#sort-submits').submit();
        });
        $('#sort-by-active').on('change', function(){
            var val = $(this).val();
            $('form#sort-submit-active').submit();
        });


        $("#search-user").on('keyup', function () {
            var user = $('#search-user').val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "admin/searchUserAutoComplete",
                data: {user: user},
                success: function (res) {
                    $("#searchResult").html(res);
                    $("#searchResult").slideDown();
                }
            });
        });
        $('#searchResult').on('click', '.search-text',function () {
            var searchKey = $(this).attr("data-value");
            $("#search-user").val(searchKey);
            $('form#search-all-form').trigger('submit');

        });

    });
</script>



</body>
</html>