<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('path');
        $this->load->model('admin_model');
        $this->load->model('common_model');
        $this->load->model('login_model');
        $this->load->library('pagination');
        $this->load->library('session');
        $this->load->helper('url');
    }

    public function index()
    {
        $data = array(
                'admin_user' => 1,
                'admin_username' => 'Admin',
                'user_type' => 1,
                'user_email' => 'admin@gmail.com',
                'validated' => 1
            );
            $this->session->set_userdata($data);
        $result = $this->login_model->validate();
        $this->load->view('login');
    }



    function checkLogin()
    {

        $check = $this->login_model->checkDeactivate();
        if ($check > 0) {
            $this->session->set_flashdata('errors', '<p style="color: #CC0000;">Your Account has been deactivated</p>');
            redirect('cRJDJK845-DFdx3');
        } else {
            $result = $this->login_model->validate();
            if ($result) {
                redirect('admin/dashboard');
            } else {
                $this->session->set_flashdata('errors', '<p style="color: #CC0000;">Incorrect username or password</p>');
                redirect('cRJDJK845-DFdx3');
            }

        }

    }

    function activateAdmin()
    {
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] == '1') {
            $data = $this->login_model->activateAdmin();
            if ($data) {
                $this->session->set_flashdata('activate_user', 'done');
            } else {
                $this->session->set_flashdata('activate_user', 'error');
            }
            redirect('admin/all-users/1');
        } else {
            redirect('cRJDJK845-DFdx3');
        }
    }

    function deactivateAdmin()
    {
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] == '1') {
            $data = $this->login_model->deactivateAdmin();
            if ($data) {
                $this->session->set_flashdata('deactivate_user', 'done');
            } else {
                $this->session->set_flashdata('deactivate_user', 'error');
            }
            redirect('admin/all-users/1');
        } else {
            redirect('cRJDJK845-DFdx3');
        }
    }

    function dashboard(){
        $result = $this->login_model->validate();
        $data = array(
                'admin_user' => 1,
                'admin_username' => 'Admin',
                'user_type' => 1,
                'user_email' => 'admin@gmail.com',
                'validated' => 1
            );
            $this->session->set_userdata($data);
         
        if ($this->checkIsValidated()) {
            $monday = date( 'Y-m-d', strtotime( 'monday this week' ) );
            $friday = date( 'Y-m-d', strtotime( 'friday this week' ) );
            $datetoday = date('Y-m-d');
            $data['total'] = $this->admin_model->getAllPassenger();
            $data['total_rider'] = $this->admin_model->getAllRider();
            $queryETW = "select count(tripID) as totalRE, ifnull(sum(totalAmount),0) as totalEarning from ride_history where rideDate like '$datetoday%'";
            $getREdetails = $this->admin_model->getDetailQuery($queryETW);
            $data['getRideThisWeek'] =  $getREdetails[0]['totalRE'];
            $data['getEarningThisWeek'] =  $getREdetails[0]['totalEarning'];

            $this->load->view('common/header', $data);
            $this->load->view('dashboard');
            $this->load->view('common/footer');
        } else {
            redirect('cRJDJK845-DFdx3');
        }
    }
    public function getRiderHistory(){

        $rideWeekStat = $this->input->get('rideWeekStat');
        if($rideWeekStat == 'Today' || empty($rideWeekStat) ){
            $datetoday = date('Y-m-d');
            $where = "where rideDate like '$datetoday%'";
        }
        if($rideWeekStat == 'Week'){
            $monday = date( 'Y-m-d', strtotime( 'monday this week' ) );
            $friday = date( 'Y-m-d', strtotime( 'friday this week' ) );
            $where = "where date_format(rideDate,'%Y-%m-%d') between '$monday' and '$friday'";
        }
        if($rideWeekStat == 'Month'){
            $Ymonth = date( 'Y-m');
            $where = "where date_format(rideDate,'%Y-%m') = '$Ymonth'";
        }
      
        $data_points = array();
        $data = $this->admin_model->getRiderHistory($where);
  
        foreach ($data as $row) {
            $point = array('y' => $row['rhourDay'] , 'a' => $row['totalTrip']);
            array_push($data_points, $point);
        }
        echo json_encode($data_points, JSON_NUMERIC_CHECK);
    }
    public function getRiderEarnings(){
        $REweekStat = $this->input->post('REweekStat');
        if($REweekStat == 'Today' || empty($REweekStat) ){
            $datetoday = date('Y-m-d');
            $where = "where date_format(rideDate, '%Y-%m-%d') = '$datetoday'";
        }
        if($REweekStat == 'Week'){
            $monday = date( 'Y-m-d', strtotime( 'monday this week' ) );
            $friday = date( 'Y-m-d', strtotime( 'friday this week' ) );
            $where = "where date_format(rideDate,'%Y-%m-%d') between '$monday' and '$friday'";
        }
        if($REweekStat == 'Month'){
            $Ymonth = date( 'Y-m');
            $where = "where date_format(rideDate,'%Y-%m') = '$Ymonth'";
        }
        $queryETW = "select count(tripID) as totalRE, ifnull(sum(totalAmount),0) as totalEarning
                    from ride_history $where";
        $getREdetails = $this->admin_model->getDetailQuery($queryETW);
        echo json_encode($getREdetails);
    }

    public function getEarningPerMonth(){
        $year = date('Y');
        $earnMonthStat = $this->input->get('earnMonthStat');
        if(empty($earnMonthStat) || $earnMonthStat == $year){
            $where = date('Y');
            $where = "where date_format(rideDate, '%Y') = '$where'";
        }else{
            $datewhere = (date('Y-').$earnMonthStat);
            $where = date('Y-m', strtotime($datewhere));
            $where = "where date_format(rideDate, '%Y-%m') = '$where'";
        }
        $query = "SELECT sum(totalAmount) as totaEarning,
                    date_format(rideDate, '%Y-%m') as rYmonth,
                    date_format(rideDate, '%Y') as rYear
                    from ride_history
                    $where
                    group by rYmonth";

        $data = $this->admin_model->getDetailQuery($query);
        $data_points = array();
        $point1 = array('m' => '0', 'a' => '0');
        array_push($data_points, $point1);
        foreach ($data as $row) {
            $point = array('m' => $row['rYmonth'] , 'a' => $row['totaEarning'], 'y' => $row['rYear']);
            array_push($data_points, $point);
        }
        echo json_encode($data_points,JSON_NUMERIC_CHECK);
      
    }
    public function checkIsValidated()
    {
        if ($this->session->userdata('validated')) {
            return true;
        } else {
            return false;
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('cRJDJK845-DFdx3', 'refresh');
    }

    function changePassword() {
        $this->load->view('common/header');
        $this->load->view('change-password');
        $this->load->view('common/footer');
    }

    function changePasswordAction() {
        $this->form_validation->set_rules('new_password', 'Password', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('change-password');
        } else {
            $this->login_model->changePasswordAction($this->session->userdata['admin_user']);
            redirect('change-password');
        }
    }

    function checkPassword() {
        $pass = $this->security->xss_clean($this->input->post('password'));
        $sql = $this->login_model->checkPass($pass, $this->session->userdata['admin_user']);
        echo json_encode(array("validation"=> $sql));
    }

    function createUser() {
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] =='1') {
            $this->load->view('common/header');
            $this->load->view('create-user');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }

    function addUser() {
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] =='1') {
            $check_email = $this->login_model->checkEmail();
            if(count($check_email) > 0) {
                $this->session->set_flashdata('add_user', 'email_exist');
            }else{
                $data = $this->login_model->addUser();
                if($data) {
                    $this->session->set_flashdata('add_user', 'done');
                } else {
                    $this->session->set_flashdata('add_user', 'error');
                }
            }
            redirect('admin/create-user');

        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }

    function editUser(){
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] =='1') {
            $user = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['user'] = $this->login_model->userData($user);
            $this->load->view('common/header');
            $this->load->view('edit-user',$data);
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }

    function editUserInfo(){
        $user =$this->input->post('user');
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] =='1') {
            $check_email = $this->login_model->checkEmailUser($user);
            if(count($check_email) > 0) {
                $this->session->set_flashdata('edit_user', 'email_exist');
            }else{
                $data = $this->login_model->editUserInfo();
                if($data) {
                    $this->session->set_flashdata('edit_user', 'done');
                } else {
                    $this->session->set_flashdata('edit_user', 'error');
                }
            }
            redirect('admin/edit-user/'.$user);

        }else{
            redirect('cRJDJK845-DFdx3');
        }

    }
    function sortUser(){
        if ($this->checkIsValidated() && ($this->session->userdata['rider_type'] =='1' || $this->session->userdata['rider_type'] =='2') )   {
            $config = array();
            $option = $this->admin_model->joinDate($this->input->post('option'));
            $config['base_url'] = base_url() . 'admin/sort-user';
            $config['total_rows'] = $this->admin_model->sortNormalUserCount($option);
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $this->admin_model->sortNormalUserCount($option);
            $data['date'] = $option;
            $data['status'] = "All";
            $data['all_riders'] = $this->admin_model->sortAllNormalUser($page, $config['per_page'],$option);
            $this->load->view('common/header', $data);
            $this->load->view('all-riders');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }

    function allUser(){
        $data = array(
                'admin_user' => 1,
                'admin_username' => 'Admin',
                'user_type' => 1,
                'user_email' => 'admin@gmail.com',
                'validated' => 1
            );
            $this->session->set_userdata($data);
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] =='1')  {
            $config = array();
            $config['base_url'] = base_url() . 'admin/all-bikes';
            $config['total_rows'] = $this->admin_model->userCount();
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $this->admin_model->userCount();
            $data['all_users'] = $this->admin_model->getAllUser($page, $config['per_page']);
            $datas = $this->admin_model->getAllUser($page, $config['per_page']);
            // $this->debug($config,1);
            $this->load->view('common/header', $data);
            $this->load->view('all-bikes');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    private function debug($msg="", $exit = false)
    {
        $today = date("Y-m-d H:i:s");

        if (is_array($msg) || is_object($msg))
        {
            echo "<hr>DEBUG ::[".$today."]<pre>\n";
            print_r($msg);
            echo "\n</pre><hr>";
        }
        else
        {
            echo "<hr>DEBUG ::[".$today."] $msg <hr>\n";
        }

        if ($exit) {
            $this->load->library('profiler');
            echo $this->profiler->run();
            exit;
        }
    }
    function viewUser(){
        $data = array(
                'admin_user' => 1,
                'admin_username' => 'Admin',
                'user_type' => 1,
                'user_email' => 'admin@gmail.com',
                'validated' => 1
            );
            $this->session->set_userdata($data);
        if ($this->checkIsValidated() && ($this->session->userdata['user_type'] =='1' || $this->session->userdata['user_type'] =='2') )   {
            $option = $this->admin_model->userStatus($this->input->post('status'));
            $config = array();
            $config['base_url'] = base_url() . 'admin/view-user/';
            $config['total_rows'] = $this->admin_model->normalUserCount($option);
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $this->admin_model->normalUserCount($option);
            $data['date'] = null;
            $data['status'] = $option;
            $data['all_riders'] = $this->admin_model->getAllNormalUser($page, $config['per_page'],$option);
            $this->load->view('common/header', $data);
            $this->load->view('all_riders');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }




    function allRider(){
        $data = array(
                'admin_user' => 1,
                'admin_username' => 'Admin',
                'user_type' => 1,
                'user_email' => 'admin@gmail.com',
                'validated' => 1
            );
            $this->session->set_userdata($data);
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] =='1')  {
            $config = array();
            $config['base_url'] = base_url() . 'admin/all-riders';



            $config['total_rows'] = $this->admin_model->userCount();
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $this->admin_model->userCount();
            $data['all_riders'] = $this->admin_model->getAllRiders($page, $config['per_page']);
            $this->load->view('common/header', $data);
            $this->load->view('all-riders');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    function addRiderInfo(){
        $location = $this->admin_model->addRider();
        $this->admin_model->addDocumentImg($location);

        redirect('admin/all-riders');
    }
    function checkFacility($rID,$faID){
        $facility = $this->admin_model->checkFacility($rID,$faID);
        return $facility;
    }

    function searchUser(){
        $data = array(
                'admin_user' => 1,
                'admin_username' => 'Admin',
                'user_type' => 1,
                'user_email' => 'admin@gmail.com',
                'validated' => 1
            );
            $this->session->set_userdata($data);
        if ($this->checkIsValidated() && ($this->session->userdata['user_type'] =='1' || $this->session->userdata['user_type'] =='2') )   {
            $config = array();
            $option = $this->admin_model->searchUser($this->input->post('search'));
            $config['base_url'] = base_url() . 'admin/sort-user';
            $config['total_rows'] = $this->admin_model->searchUserCount($option);
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $this->admin_model->searchUserCount($option);
            $data['date'] = '';
            $data['status'] = "All";
            $data['search'] = $option;
            $data['all_users'] = $this->admin_model->searchAllNormalUser($page, $config['per_page'],$option);
            $this->load->view('common/header', $data);
            $this->load->view('rider');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }



    function searchBike(){
        $data = array(
                'admin_user' => 1,
                'admin_username' => 'Admin',
                'user_type' => 1,
                'user_email' => 'admin@gmail.com',
                'validated' => 1
            );
            $this->session->set_userdata($data);
        if ($this->checkIsValidated()  )   {
            $config = array();
            $option = $this->admin_model->searchBike($this->input->post('search'));
            $config['base_url'] = base_url() . 'admin/sort-user';
            $config['total_rows'] = $this->admin_model->searchBikeCount($option);
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $this->admin_model->searchBikeCount($option);
            $data['date'] = '';
            $data['status'] = "All";
            $data['search'] = $option;
            $data['all_bikes'] = $this->admin_model->searchAllNormalBikes($page, $config['per_page'],$option);
            $this->load->view('common/header', $data);
            $this->load->view('bikes');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    function searchBikeAndRider(){
        $data = array(
                'admin_user' => 1,
                'admin_username' => 'Admin',
                'user_type' => 1,
                'user_email' => 'admin@gmail.com',
                'validated' => 1
            );
            $this->session->set_userdata($data);
        if ($this->checkIsValidated()  )   {
            $option = "";
            $total_rows = "";
            $total_db_rows = "";
            $per_page = "";
            $page = "";
            $search_category = $this->input->post('search_category');
            $config = array();
            if($search_category == 'bikes'){
                $option = $this->admin_model->searchBike($this->input->post('search'));
                $total_rows = $this->admin_model->searchBikeCount($option);
                $total_db_rows =$this->admin_model->searchBikeCount($option);
                $per_page = 50;
                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $data['all_bikes'] = $this->admin_model->searchAllNormalBikes($page, $per_page,$option);
            }
            if($search_category == 'riders'){
                $option = $this->admin_model->searchUser($this->input->post('search'));
                $total_rows = $this->admin_model->searchUserCount($option);
                $total_db_rows =$this->admin_model->searchUserCount($option);
                $per_page = 50;
                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $data['all_users'] = $this->admin_model->searchAllNormalUser($page, $per_page,$option);
            }
            $config['base_url'] = base_url() . 'admin/sort-user';
            $config['total_rows'] = $total_rows;
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $total_db_rows;
            $data['date'] = '';
            $data['status'] = "All";
            $data['search'] = $option;
            

            if($search_category == 'bikes'){
                $this->load->view('common/header', $data);
                $this->load->view('bikes');
                $this->load->view('common/footer');
            }elseif($search_category == 'riders'){
                $this->load->view('common/header', $data);
                $this->load->view('rider');
                $this->load->view('common/footer');
            }else{
                redirect('admin/dashboard');
            }
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    // sort location by added date
    function sortLocation(){
        $data = array(
                'admin_user' => 1,
                'admin_username' => 'Admin',
                'user_type' => 1,
                'user_email' => 'admin@gmail.com',
                'validated' => 1
            );
            $this->session->set_userdata($data);
        if ($this->checkIsValidated()) {
            $option = 'All';
            $from = $this->admin_model->sortFrom($this->input->post('from'));
            $to = $this->admin_model->sortTo($this->input->post('to'));
            $config = array();
            $config['base_url'] = base_url() . 'admin/sort-all-locations/';
            $config['total_rows'] = $this->admin_model->getSortLocationCount($from,$to);
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $this->admin_model->getSortLocationCount($from,$to);
            $data['park'] = $option;
            $data['from'] = $from;
            $data['to'] = $to;
            $data['type'] = '';
            $data['all_location'] = $this->admin_model->getSortLocation($page, $config['per_page'],$from,$to);

            $this->load->view('common/header', $data);
            $this->load->view('all-location');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    function editRider(){
        $data = array(
                'admin_user' => 1,
                'admin_username' => 'Admin',
                'user_type' => 1,
                'user_email' => 'admin@gmail.com',
                'validated' => 1
            );
            $this->session->set_userdata($data);
        if ($this->checkIsValidated()) {
            $location = $this->uri->segment(3);
            // $data['location'] = $this->admin_model->getLocationDetails($location);
            $data['location'] = $this->admin_model->GetInfoByRow('rider','rID', array('rID' => $location));
            $data['location_img'] = $this->admin_model->getDocumentImages($location);
            $data['internal_bikes'] = $this->admin_model->getInternalBike();
            $this->load->view('common/header', $data);
            $this->load->view('edit-rider');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    function pendingRider(){
    
           $data = array(
                'admin_user' => 1,
                'admin_username' => 'Admin',
                'user_type' => 1,
                'user_email' => 'admin@gmail.com',
                'validated' => 1
            );
            $this->session->set_userdata($data);
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] =='1')  {
            $config = array();
            $config['base_url'] = base_url() . 'admin/pending-rider';



            $config['total_rows'] = $this->admin_model->userCount();
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $this->admin_model->userCount();
            
            $rdocID = $this->admin_model->getAllRiderStatus3();
            // $wheres = array($rdocID => $rdocID);
            $data['all_riders'] = $this->admin_model->getAllRidersPending($page, $config['per_page'], $rdocID);
            $this->load->view('common/header', $data);
            $this->load->view('all-pending-rider');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    public function ActionpendingRider(){
        $data = array(
                'admin_user' => 1,
                'admin_username' => 'Admin',
                'user_type' => 1,
                'user_email' => 'admin@gmail.com',
                'validated' => 1
            );
            $this->session->set_userdata($data);
        if ($this->checkIsValidated()) {
            $location = $this->uri->segment(3);
            // $data['location'] = $this->admin_model->getLocationDetails($location);
            $data['location'] = $this->admin_model->GetInfoByRow('rider','rID', array('rID' => $location));
            $data['location_img'] = $this->admin_model->getDocumentImages($location);
            $data['internal_bikes'] = $this->admin_model->getInternalBike();
            $this->load->view('common/header', $data);
            $this->load->view('pending-rider');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    public function delete()
    {

        $id = $this->uri->segment(3);

        if (empty($id)) {
            show_404();
        }

        $news_item = $this->admin_model->get_news_by_id($id);

        if ($news_item['user_id'] != $this->session->userdata('user_id')) {
            $currentClass = $this->router->fetch_class(); // class = controller
            redirect(site_url($currentClass));
        }

        $this->admin_model->delete_news($id);
        redirect( base_url() . 'index.php/news');
    }

    public function delete_book()
    {
        //get data from myview
        $bikeId = $this->input->post('bID');
        $this->load->model('Admin_model');
        $this->admin_model->book_delete($bikeId);
        echo "Book Deleted Successfully!";
    }

    function viewBike(){
        $data = array(
                'admin_user' => 1,
                'admin_username' => 'Admin',
                'user_type' => 1,
                'user_email' => 'admin@gmail.com',
                'validated' => 1
            );
            $this->session->set_userdata($data);
        if ($this->checkIsValidated()) {
            $location = $this->uri->segment(3);
            $data['location'] = $this->admin_model->getBikeDetails($location);
            $data['location_img'] = $this->admin_model->getDocumentImages($location);
//            $data['location_sinhala'] = $this->admin_model->getLocationSinhala($location);
//            $data['location_tamil'] = $this->admin_model->getLocationTamil($location);
//            $data['location_facility'] = $this->admin_model->getLocationFacility($location);
//            $data['opening_hour_sinhala'] = $this->admin_model->getOpeningHourSin($location);
//            $data['opening_hour_tamil'] = $this->admin_model->getOpeningHourTam($location);
//            $data['opening_hour'] = $this->admin_model->getOpeningHour($location);
//            $data['tags'] = $this->admin_model->getLocationTag($location);
            $this->load->view('common/header', $data);
            $this->load->view('view-bike');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }

    function addRider(){
        $data = array(
                'admin_user' => 1,
                'admin_username' => 'Admin',
                'user_type' => 1,
                'user_email' => 'admin@gmail.com',
                'validated' => 1
            );
            $this->session->set_userdata($data);
//        $data['facility'] = $this->admin_model->getAllFacility();
//        $data['location'] = $this->admin_model->getLastLocation();
//        $data['bikes'] = $this->admin_model->addLocation();
        $data['internal_bikes'] = $this->admin_model->getInternalBike();

        $this->load->view('common/header');
        $this->load->view('add-rider',$data);
        $this->load->view('common/footer');

    }
    function addBikes(){
//        $data['facility'] = $this->admin_model->getAllFacility();
//        $data['location'] = $this->admin_model->getLastLocation();
//        $data['bikes'] = $this->admin_model->addLocation();
    $data['internal_riders'] = $this->admin_model->getInternalRider();
    $this->load->view('common/header');
    $this->load->view('add-bikes',$data);
    $this->load->view('common/footer');

    }
    function editBikes($id=null){
//        $data['facility'] = $this->admin_model->getAllFacility();
//        $data['location'] = $this->admin_model->getLastLocation();
//        $data['bikes'] = $this->admin_model->addLocation();
    $location = $this->uri->segment(3);
    $data['location'] = $this->admin_model->getLocationDetails($location);
    $data['location_img'] = $this->admin_model->getDocumentImages($location);
    $data['internal_riders'] = $this->admin_model->getInternalRider();
    $data['edit_bike_list'] = $this->admin_model->GetInfoByRow('bikes', 'bID', array('bID' => $id));
    $this->load->view('common/header');
    $this->load->view('edit-bikes',$data);
    $this->load->view('common/footer');

    }
    function editBikesProcess(){
       
        $form_data = array(
                'bBrand' => $this->input->post("brand"),
                'bType' => $this->input->post("type"),
                'bChassisNumber' => $this->input->post('chassis_number'),
                'bVehicleNumber' => $this->input->post('vehicle_number'),
                'bAssignRider' => $this->input->post('internal-rider'),
                'bInsuranceExpiry' => $this->input->post('insurance_exp')
        );
        $id = $this->input->post('hddbID');
        $this->admin_model->Manage('bikes',$form_data, array('bID' => $id),'update');
        redirect('admin/all-bikes/1',$data);
    }
    function addLocation1(){
        $data['bikes'] = $this->admin_model->addBikes();

        redirect('admin/all-bikes/1',$data);
    }

    function addRider1(){
        $document = $this->admin_model->addRider();
        $this->admin_model->addDocumentImg($document);

        redirect('admin/all-riders/1');
    }
    function editRiderProcess(){
        $equipments = $this->input->post('equipments');
        $equip = '';
        foreach ($equipments as $eq) {
            $equip .= $eq.',';
        }
        $form_data = array(
            'rName' => $this->input->post('rider_name'),
            'rPhone' => $this->input->post('contact_no'),
            'rEmail' => $this->input->post('rider_email'),
            'rEpfNumber' => $this->input->post('rider_epfno'),
            'rSalary' => $this->input->post('rider_salary'),
            'rEmiNumber' => $this->input->post('phone_emi'),
            'rEquipments' => rtrim($equip, ','),
            'rInitialDeposite' => $this->input->post('initial_deposit'),
            'rBykerPhoneNumber' => $this->input->post('byker_phone_number'),
            'rLastName' => $this->input->post('last_name'),
            'rInExRiders' => $this->input->post('rInExRiders')
            // 'driving_expe=' => $this->input->post('driving_expe')
        );
        $where = array('rID' => $this->input->post("hddrID"));
        $this->admin_model->Manage('rider',$form_data, $where, 'update');
        $this->admin_model->RDelete('rider_document', $where);
        $this->admin_model->addDocumentImg($this->input->post("hddrID"));
       redirect('admin/all-riders/1');
    }
    public function approve_reject_process(){
        
        $action = $this->input->post('btn_submit');
        if($action == 'Accept'){
            $rdStatus = 3;
        }else{
            $rdStatus = 0;
        }
        $form_data = array(
            'rdStatus' => $rdStatus,
            'remarks' => $this->input->post('remarks')
        );
        $hddrdID = $this->input->post('hddrdID');
        $where = array('rdID' => $hddrdID);
        $this->admin_model->Manage('rider_document',$form_data, $where, 'update');
        $hddrID = $this->input->post('hddrID'); // location
        redirect("admin/action-pending-rider/$hddrID");
    }
    public function promo_codes($action=null){
        $data = array(
                'admin_user' => 1,
                'admin_username' => 'Admin',
                'user_type' => 1,
                'user_email' => 'admin@gmail.com',
                'validated' => 1
            );
        $this->session->set_userdata($data);
        
        switch (strtoupper($action)) {
            case 'ADD':
                $data['title'] = 'Promo Codes';
                $this->load->view('common/header');
                $this->load->view('add-promo-codes',$data);
                $this->load->view('common/footer');
            break;
            
            default:
                $data['promo_list'] = $this->admin_model->GetAllInfo('promo_code','idpc',array());
                $this->load->view('common/header');
                $this->load->view('promo-codes',$data);
                $this->load->view('common/footer');
            break;
        }

        
    }
    public function addPromocode(){
        $passenger = $this->input->post('all_passenger');
            $form_data = array(
                'discount_type' => $this->input->post('discount_type'),
                'discount' => $this->input->post('discount'),
                'start_date' => $this->input->post('start_date'),
                'promo_code' => $this->input->post('promo_code'),
                'expiration' => $this->input->post('expiration'),
                'message' => $this->input->post('message'),
                'recipients' => $this->input->post('recipients'),
                'ride_total' => $this->input->post('ride_total'),
                'all_passenger' => ($passenger) ? 1 : 0,
                'startDate1' => $this->input->post('startDate1'),
                'endDate1' => $this->input->post('endDate1'),
                'status' => 1
            );
        $this->admin_model->SaveForm('promo_code',$form_data);
        redirect("admin/promo_codes");
    }
}