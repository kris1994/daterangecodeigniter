<!DOCTYPE html>
<html dir="ltr" lang="en-US">
   <head>
      <meta charset="UTF-8" />
      <title>Report</title>
      <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
   </head>
   <body style="margin: 60px 0">
  
        <h1 style="margin: 0 0 20px 0">Report</h1>
    <form action="<?php echo site_url('DateRangePicker/generateReports')?>" method="post">
        <div class="row">
        	<table border="1">
        		<thead><tr>
        				<th>rID</th>
						<th>rtID</th>
						<th>rName</th>
						<th>rEmail</th>
						<th>rPassword</th>
						<th>rJoinDate</th>
						<th>rStatus</th>
						<th>rPhone</th>
						<th>rLastView</th>
						<th>rActivationCode</th>
						<th>rCompleteness</th>
						<th>rRiderType</th>
						<th>bID</th>
						<th>rRatings</th>
						<th>rTypes</th>
						<th>rLastName</th>
						<th>rAssignBike</th>
						<th>rInExRiders</th>
						<th>rInitialDeposite</th>
						<th>rEmiNumber</th>
						<th>rBykerPhoneNumber</th>
						<th>rEquipments</th>
						<th>rSalary</th>
						<th>rEpfNumber</th>
						</tr>
    			</thead>
	        	<tbody>
	        	<?php foreach ($report_list as $row) { ?>
	        		<tr>
	        			<td><?php echo $row['rID']?></td>
	        			<td><?php echo $row['rtID']?> </td>
						<td><?php echo $row['rName']?> </td>
						<td><?php echo $row['rEmail']?> </td>
						<td><?php echo $row['rPassword']?> </td>
						<td><?php echo $row['rJoinDate']?> </td>
						<td><?php echo $row['rStatus']?> </td>
						<td><?php echo $row['rPhone']?> </td>
						<td><?php echo $row['rLastView']?> </td>
						<td><?php echo $row['rActivationCode']?> </td>
						<td><?php echo $row['rCompleteness']?> </td>
						<td><?php echo $row['rRiderType']?> </td>
						<td><?php echo $row['bID']?></td>
						<td><?php echo $row['rRatings']?></td>
						<td><?php echo $row['rTypes']?></td>
						<td><?php echo $row['rLastName']?></td>
						<td><?php echo $row['rAssignBike']?></td>
						<td><?php echo $row['rInExRiders']?></td>
						<td><?php echo $row['rInitialDeposite']?></td>
						<td><?php echo $row['rEmiNumber']?></td>
						<td><?php echo $row['rBykerPhoneNumber']?></td>
						<td><?php echo $row['rEquipments']?></td>
						<td><?php echo $row['rSalary']?></td>
						<td><?php echo $row['rEpfNumber']?></td>
	        		</tr>
	        	<?php }?>
	        	</tbody>
	        </table>
        </div>
        
    </form>

   

   </body>
</html>
