<!DOCTYPE html>
<html dir="ltr" lang="en-US">
   <head>
      <meta charset="UTF-8" />
      <title>A date range picker for Bootstrap</title>
      <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('css/daterangepicker.css')?>" />
      <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
      <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url('js/moment.js')?>"></script>
      <script type="text/javascript" src="<?php echo base_url('js/daterangepicker.js')?>"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
   </head>
   <body style="margin: 60px 0">

   <div class="container">

        <h1 style="margin: 0 0 20px 0">Configuration Builder</h1>
    <form id="generateReports" action="<?php echo site_url('DateRangePicker/generateReports')?>" method="post">
        <div class="well configurator">
        <div class="row">
	        <div class="col-md-6">
	        	<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
				    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
				    <span></span> <b class="caret"></b>
				</div>
			</div>
        </div>
        <div class="form-group">
				<input type="hidden" name="daterange_from" id="daterange_from" value="" class="form-control">
				<input type="hidden" name="daterange_to" id="daterange_to" value="" class="form-control">
		</div>
      </div>
    </form>
  </div>

      <style type="text/css">
      .demo { position: relative; }
      .demo i {
        position: absolute; bottom: 10px; right: 24px; top: auto; cursor: pointer;
      }
      </style>
<script type="text/javascript">
	$(function() {
	    var start = moment().subtract(29, 'days');
	    var end = moment();

	    function cb(start, end) {
	        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	        $("#daterange_from").val(start.format('MMMM D, YYYY'));
	        $("#daterange_to").val(end.format('MMMM D, YYYY'));
	    }

	    $('#reportrange').daterangepicker({
	        startDate: start,
	        endDate: end,
	        ranges: {
	           'Today': [moment(), moment()],
	           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	           'This Month': [moment().startOf('month'), moment().endOf('month')],
	           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	        }
	    }, cb);

	    cb(start, end);
	    $(".applyBtn").click(function(){
			$("#generateReports").submit();
		});
		$(".ranges").on('click', 'li', function(){
			if($(this).text() != 'Custom Range'){
				$("#generateReports").submit();
			}
		})
	});
	
</script>
   

   </body>
</html>
