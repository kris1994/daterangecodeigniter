<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DateRangePicker extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model('DateRange_model', 'dr');
    }
	public function index()
	{
		$this->load->view('DateRangePicker_view');
	}
	public function generateReports(){
		$daterange_from = $this->input->post('daterange_from');
    	$daterange_to = $this->input->post('daterange_to');
    	$date_from = date("Y-m-d", strtotime($daterange_from));
    	$date_to = date("Y-m-d", strtotime($daterange_to));
    	$data['report_list'] = $this->dr->dbgetQuery($date_from, $date_to);
    	$this->load->view('report_view', $data);
	}
}
