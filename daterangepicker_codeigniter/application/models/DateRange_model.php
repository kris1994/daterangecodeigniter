<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DateRange_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function dbgetQuery($date_from=null, $date_to=null){
        $this->db->where('rJoinDate >=', $date_from);
        $this->db->where('rJoinDate <=', $date_to);
        return $this->db->get('rider')->result_array();
    }
}

?>